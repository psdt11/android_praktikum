package com.example.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLOutput;
import java.util.Enumeration;
import java.util.Hashtable;

public class ServerActivity extends AppCompatActivity {

    public static String SERVER_IP = "server";
    public static int SERVER_PORT = 8080;
    public static boolean SERVER = true;
    public static String NICKNAME = "muster";

    private Handler handler;

    //Thread ConnectionThread = null;
    Thread serverThread =  null;
    Thread clientThread = null;

    private ServerSocket serverSocket;
    private Socket tmpClientSocket;
    private Socket clientSocket;

    private TextView tv_chat;
    private EditText et_chat;
    private ImageButton btn_send;

    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        btn_send = findViewById(R.id.btn_send);
        tv_chat = findViewById(R.id.tv_chat);
        et_chat = findViewById(R.id.et_chat);
        handler = new Handler();

        if(getIntent().hasExtra(String.valueOf(SERVER))) {
            SERVER = getIntent().getBooleanExtra(String.valueOf(SERVER),true);
        }

        if(getIntent().hasExtra(String.valueOf(SERVER_PORT))) {
            SERVER_PORT = getIntent().getIntExtra(String.valueOf(SERVER_PORT), 8080);
        }

        if(getIntent().hasExtra(SERVER_IP)) {
            SERVER_IP = getIntent().getStringExtra(SERVER_IP);
        }

        if(getIntent().hasExtra(NICKNAME)) {
            NICKNAME = getIntent().getStringExtra(NICKNAME);
        }

        serverThread = new Thread(new ServerThread());
        serverThread.start();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = et_chat.getText().toString().trim();
                showMessage(NICKNAME + ": " + msg);
                if (msg.length() > 0) {
                    sendMessage(msg);
                }
                et_chat.setText("");
            }
        });

    }

    public void showMessage(String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_chat.append(msg);
            }
        });
    }

    private void sendMessage(final String msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                PrintWriter out = null;
                try {
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(tmpClientSocket.getOutputStream())));
                    out.println(msg);
                    out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    class ServerThread implements Runnable {

        @Override
        public void run() {
            Socket socket;
            try {
                serverSocket = new ServerSocket(SERVER_PORT);
            } catch (IOException e) {
                e.printStackTrace();
                showMessage("Server could not be started : " + e.getMessage());
            }

            if(serverSocket != null) {
                while(!Thread.currentThread().isInterrupted()) {
                    try {
                        socket = serverSocket.accept();
                        CommuneThread communeThread = new CommuneThread(socket);
                        new Thread(communeThread).start();
                    } catch (IOException e) {
                        e.printStackTrace();
                        showMessage("Could not connect to client : " + e.getMessage());
                    }
                }
            }
            showMessage("Server started! \n");
        }

    }


    class CommuneThread implements Runnable {
        private Socket socket;
        private BufferedReader input;

        public CommuneThread(Socket socket) {
            this.socket = socket;
            tmpClientSocket = socket;
            try {
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    String msg = input.readLine();
                    System.out.println("[SERVER-INPUT]: " + msg);
                    showMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}