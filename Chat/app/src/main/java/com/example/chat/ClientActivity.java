package com.example.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientActivity extends AppCompatActivity {

    public static String SERVER_IP = "server";
    public static int SERVER_PORT = 8080;
    public static String NICKNAME = "muster";

    private ClientThread clientThread;
    private Thread thread;
    private Handler handler;

    private TextView tv_chat;
    private EditText et_chat;
    private ImageButton btn_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        btn_send = findViewById(R.id.btn_send);
        tv_chat = findViewById(R.id.tv_chat);
        et_chat = findViewById(R.id.et_chat);
        handler = new Handler();

        if(getIntent().hasExtra(String.valueOf(SERVER_PORT))) {
            SERVER_PORT = getIntent().getIntExtra(String.valueOf(SERVER_PORT), 8080);
        }

        if(getIntent().hasExtra(SERVER_IP)) {
            SERVER_IP = getIntent().getStringExtra(SERVER_IP);
        }

        if(getIntent().hasExtra(NICKNAME)) {
            NICKNAME = getIntent().getStringExtra(NICKNAME);
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = et_chat.getText().toString().trim();
                showMessage(NICKNAME + ": " + msg);
                if (msg.length() > 0) {
                    clientThread.sendMessage(msg);
                }
                et_chat.setText("");
            }
        });

        clientThread = new ClientThread();
        thread = new Thread(clientThread);
        thread.start();
    }

    public void showMessage(String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_chat.append(msg);
            }
        });
    }

    class ClientThread implements Runnable {

        private Socket clientSocket;
        private BufferedReader input;

        @Override
        public void run() {
            try {
                showMessage("Socket connecting...");
                clientSocket = new Socket(SERVER_IP, SERVER_PORT);
                if(clientSocket.isBound()) {
                    showMessage("Socket connected!\n");
                }
                while (true) {
                    input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String msg = input.readLine();
                    showMessage("Server: " + msg);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void sendMessage(final String msg) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())));
                        System.out.println("[sendMessage] --> " + msg);
                        out.println(msg);
                        out.flush();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}