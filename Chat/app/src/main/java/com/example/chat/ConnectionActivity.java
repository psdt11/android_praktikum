package com.example.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionActivity extends AppCompatActivity {

    public static PrintWriter output;
    public static BufferedReader input;

    private EditText et_ip;
    private EditText et_port;
    private EditText et_name;
    private Button btn_connect;
    private Thread serverThread = null;
    private boolean server;
    private ServerSocket serverSocket;
    private Socket socket;
    public static String SERVER_IP = "ip";
    public static int SERVER_PORT = 8080;
    public static String NICKNAME = "muster";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        et_ip = findViewById(R.id.et_ip);
        et_port = findViewById(R.id.et_port);
        et_name = findViewById(R.id.et_name);
        btn_connect = findViewById(R.id.btn_connect);

        if (getIntent().hasExtra(SERVER_IP)) {
            SERVER_IP = getIntent().getStringExtra(SERVER_IP);
            et_ip.setText(SERVER_IP);
            et_ip.setFocusable(false);
            et_port.setText(String.valueOf(SERVER_PORT));
            et_port.setFocusable(false);
            server = true;
        } else {
            server = false;
        }

        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!server) {
                    SERVER_IP = et_ip.getText().toString();
                    SERVER_PORT = Integer.parseInt(et_port.getText().toString());
                }
                    NICKNAME = et_name.getText().toString();
                    openChatActivity();
            }
        });
    }

    private void openChatActivity() {
        Intent i = null;
        if(server) {
            i = new Intent(this, ServerActivity.class);
        } else {
            i = new Intent(this, ClientActivity.class);
        }
        i.putExtra(ServerActivity.SERVER_IP, SERVER_IP);
        i.putExtra(String.valueOf(ServerActivity.SERVER_PORT), SERVER_PORT);
        i.putExtra(String.valueOf(ServerActivity.SERVER),server);
        i.putExtra(ServerActivity.NICKNAME,NICKNAME);
        startActivity(i);
    }
}