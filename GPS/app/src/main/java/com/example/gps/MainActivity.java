package com.example.gps;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnA, btnB, btnComp;
    TextView tvCom, tvALong, tvALat, tvBLong, tvBLat;
    boolean aSet, bSet;
    double alat, alon, blat, blon;


    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 123;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnA = findViewById(R.id.btPositionA);
        btnB = findViewById(R.id.btPositionB);
        btnComp = findViewById(R.id.btCompass);
        tvALat = findViewById(R.id.tvLatA);
        tvALong = findViewById(R.id.tvLongA);
        tvBLat = findViewById(R.id.tvLatB);
        tvBLong = findViewById(R.id.tvLongB);
        tvCom = findViewById(R.id.tvCompass);
        aSet = false;
        bSet = false;


        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
        } else {
            LocationManager m = getSystemService(LocationManager.class);
            List<String> providers = m.getAllProviders();
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            String p = m.getBestProvider(criteria, true);
            LocationListener ll = new LocationListener() {
                @Override
                public void onLocationChanged(@NonNull Location location) {

                    if(!aSet) {
                        alat = location.getLatitude();
                        alon = location.getLongitude();
                        tvALat.setText("Lat: " + Location.convert(location.getLatitude(), Location.FORMAT_DEGREES));
                        tvALong.setText("Long: " + Location.convert(location.getLongitude(), Location.FORMAT_DEGREES));
                    }else if(!bSet){
                        tvBLat.setText("Lat: " + Location.convert(location.getLatitude(), Location.FORMAT_DEGREES));
                        tvBLong.setText("Long: " + Location.convert(location.getLongitude(), Location.FORMAT_DEGREES));
                        blat = location.getLatitude();
                        blon = location.getLongitude();
                    }
                }
            };
            m.requestLocationUpdates(p, 500, 0, ll);
        }


    }
    public void setA(View v){
        aSet=!aSet;
    }
    public void setB(View v){
        bSet=!bSet;
    }
    public void calcCompass(View v){
        if (alat<blat){//North
            if(alon<blon){//East
                if((blat-alat)<(blon-alon)){
                    tvCom.setText("You're going east(ish)");
                }else{
                    tvCom.setText("You're going north(ish)");
                }
            }else{//West
                if((blat-alat)<(alon-blon)){
                    tvCom.setText("You're going west(ish)");
                }else{
                    tvCom.setText("You're going north(ish)");
                }
            }
        }else{//South
            if(alon<blon){//East
                if((alat-blat)<(blon-alon)){
                    tvCom.setText("You're going east(ish)");
                }else{
                    tvCom.setText("You're going south(ish)");
                }
            }else{//West
                if((alat-blat)<(alon-blon)){
                    tvCom.setText("You're going west(ish)");
                }else{
                    tvCom.setText("You're going south(ish)");
                }
            }
        }
    }

}
