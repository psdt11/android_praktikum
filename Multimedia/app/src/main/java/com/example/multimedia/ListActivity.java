package com.example.multimedia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ListActivity extends AppCompatActivity {

    public static String SIZE = "size";
    public static String FLOUR = "flour";
    public static String INGREDIENTS = "ingredients";
    private String[] ingredients;

    private TextView tv_input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        if(getIntent().hasExtra(SIZE)) {
            SIZE = getIntent().getStringExtra(SIZE);
        }

        if (getIntent().hasExtra(FLOUR)) {
            FLOUR = getIntent().getStringExtra(FLOUR);
        }

        if(getIntent().hasExtra(INGREDIENTS)) {
            INGREDIENTS = getIntent().getStringExtra(INGREDIENTS);
        }

        tv_input = findViewById(R.id.tv_input);

        ingredients = INGREDIENTS.split(" ");

        tv_input.append("Größe: " + SIZE + "\n");
        tv_input.append("Mehl: " + FLOUR + "\n");
        tv_input.append("Zutaten: ");

        for (int i = 0; i < ingredients.length; i++) {
            tv_input.append(ingredients[i] + "\n");
        }
    }
}