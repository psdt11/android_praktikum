package com.example.multimedia;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final int RQ_CHECK_TTS_DATA = 1;
    private static final int RQ_VOICE_RECOGNITION = 2;
    private static final String TAG = MainActivity.class.getSimpleName();

    private static String SIZE = "size";
    private static String FLOUR= "flour";
    private static String INGREDIENTS = "ingredients";

    private int state = 0;

    private String NAME = "Horst";

    private TextToSpeech tts;

    private Button btn_go;
    private EditText et_name;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tts = null;
        Intent intent = new Intent();
        intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, RQ_CHECK_TTS_DATA);


    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "PAUSED!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "RESUMED!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(tts != null) {
            tts.shutdown();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RQ_CHECK_TTS_DATA) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onInit(int status) {
                        onInit1(status);
                    }
                });
            }
        } else if(requestCode == RQ_VOICE_RECOGNITION && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(matches.size() > 0 ) {
                Log.d(TAG, matches.get(0));
            }
            switch (state) {
                case 2:
                    SIZE = matches.get(0);
                    break;
                case 4:
                    FLOUR = matches.get(0);
                    break;
                case 6:
                    INGREDIENTS = matches.get(0);
                    break;
            }
            state++;
            switch (state) {
                case 3:
                    askFlour();
                    break;
                case 5:
                    askIngredients();
                    break;
                case 7:
                    openListActivity();
                    break;
            }
        } else {
            Intent installIntent = new Intent();
            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
            startActivity(installIntent);
            finish();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onInit1(int status) {
        if(status != TextToSpeech.SUCCESS) {
            finish();
        } else {
            setContentView(R.layout.activity_main);

            btn_go = findViewById(R.id.btn_go);
            et_name = findViewById(R.id.et_name);

            btn_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    text2speech(et_name.getText().toString());
                }
            });

            tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.d(TAG, "onStart --> " + utteranceId);
                }

                @Override
                public void onDone(String utteranceId) {
                    Log.d(TAG, "onDone --> " + utteranceId);
                    state++;
                    switch (state) {
                        case 1:
                            askSize();
                            break;
                        case 3:
                            askFlour();
                            break;
                        case 5:
                            askIngredients();
                            break;
                        case 2:
                        case 4:
                        case 6:
                            speech2text();
                            break;
                    }
                }

                @Override
                public void onError(String utteranceId) {
                    Log.d(TAG, "onError()");
                }
            });
        }
    }

    public void speech2text() {
        startVoiceRecognitionActivity();
    }

    public void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Bitte jetzt sprechen!");
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "de-DE");
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        startActivityForResult(intent,RQ_VOICE_RECOGNITION);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void text2speech(String name) {
        String text = "Hallo " + name;
        tts.setLanguage(Locale.GERMAN);
        tts.speak(text, TextToSpeech.QUEUE_FLUSH,null, Long.toString(System.currentTimeMillis()));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void askSize() {
        tts.setLanguage(Locale.GERMAN);
        tts.speak("Welche Größe soll die Pizza haben? Klein, mittel oder Groß?", TextToSpeech.QUEUE_FLUSH,null,Long.toString(System.currentTimeMillis()));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void askFlour() {
        tts.setLanguage(Locale.GERMAN);
        tts.speak("Welches Mehl soll für den Teig verwendet werden? Weizen oder Vollkorn?", TextToSpeech.QUEUE_FLUSH,null, Long.toString(System.currentTimeMillis()));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void askIngredients() {
        tts.setLanguage(Locale.GERMAN);
        tts.speak("Welche Zutaten sollen auf der Pizza sein?", TextToSpeech.QUEUE_FLUSH, null, Long.toString(System.currentTimeMillis()));
    }

    public void openListActivity() {
        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra(ListActivity.SIZE, SIZE);
        intent.putExtra(ListActivity.FLOUR, FLOUR);
        intent.putExtra(ListActivity.INGREDIENTS, INGREDIENTS);
        startActivity(intent);
    }
}