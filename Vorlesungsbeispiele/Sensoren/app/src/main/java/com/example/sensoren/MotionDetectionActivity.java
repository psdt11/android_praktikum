package com.example.sensoren;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class MotionDetectionActivity  extends AppCompatActivity {

    CheckBox cb;
    SensorManager sensorManager;
    Sensor sensor1;
    TriggerEventListener l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motiondetection);
        cb = findViewById(R.id.checkBox);

        sensorManager = getSystemService(SensorManager.class);
        sensor1 = sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
        l = new TriggerEventListener() {
            @Override
            public void onTrigger(TriggerEvent triggerEvent) {
                cb.setChecked(true);
                cb.setText(Calendar.getInstance().getTime().toString());
                sensorManager.requestTriggerSensor(l,sensor1);
            }
        };
        sensorManager.requestTriggerSensor(l,sensor1);
    }


}
