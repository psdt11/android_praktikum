package com.example.sensoren;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SensorManager sensorManager;

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = findViewById(R.id.textview);
        sensorManager = getSystemService(SensorManager.class);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        /*
        for (Sensor s: sensors) {
            tv.append(s.getName() + "\t\t" + s.getVendor() + "\t\t" + s.getVersion()+ "\n");
        }
         */
    }

    public void showLightSensor(View v){
        Intent intent = new Intent(this, LightValueActivity.class);
        startActivity(intent);
    }

    public void motiondetection (View v){
        Intent intent = new Intent(this, MotionDetectionActivity.class);
        startActivity(intent);
    }

    public void gps (View v){
        Intent intent = new Intent(this, GPSActivity.class);
        startActivity(intent);
    }
}
