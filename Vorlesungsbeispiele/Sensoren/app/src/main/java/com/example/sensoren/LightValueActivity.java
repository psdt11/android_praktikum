package com.example.sensoren;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class LightValueActivity extends AppCompatActivity {

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lightvalue);
        tv = findViewById(R.id.textView);

        SensorManager sensorManager = getSystemService(SensorManager.class);
        Sensor sensor1 = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (sensor1 != null) {
            SensorEventListener listener = new SensorEventListener(){
                @Override
                public void onAccuracyChanged(Sensor sensor, int acuracy){}

                @Override
                public void onSensorChanged(SensorEvent event){
                    tv.setText(String.valueOf(event.values[0]));
                }
            };
            sensorManager.registerListener(listener, sensor1, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

}
