package com.example.sensoren;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.List;


public class GPSActivity extends AppCompatActivity {

    TextView tv1;
    TextView tv2;
    TextView tv3;

    double l1 = 0;
    double l2 = 0;

    double b1 = 0;
    double b2 = 0;

    double d1 = 0;
    double d2 = 0;
    double d3 = 0;
    int i = 0;

    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        tv1 = findViewById(R.id.textView2);
        tv2 = findViewById(R.id.textView4);
        tv3 = findViewById(R.id.textView3);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
        } else {
            LocationManager m = getSystemService(LocationManager.class);
            List<String> providers = m.getAllProviders();
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            String p = m.getBestProvider(criteria, true);
            LocationListener l = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    tv1.setText("Breite: " + Location.convert(location.getLatitude(), Location.FORMAT_DEGREES));
                    tv2.setText("Länge: " + Location.convert(location.getLongitude(), Location.FORMAT_DEGREES));
                    if(i>3) b1=b2;
                    b2= location.getLatitude();
                    if(i>3) l1=l2;
                    l2= location.getLongitude();
                    d1=b2-b1;
                    d2=l2-l1;
                    if(i>3) {
                        d3 = Math.sqrt((d1*d1)+(d2*d2));
                        tv3.setText(String.valueOf(Math.round((d3*(40000000/360))))+" m");
                    }
                    i++;
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                }

                @Override
                public void onProviderDisabled(String s) {
                }
            };
            m.requestLocationUpdates(p, 3000, 0, l);
        }
    }
}
