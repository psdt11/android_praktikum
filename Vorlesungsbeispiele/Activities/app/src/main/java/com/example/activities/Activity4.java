package com.example.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Activity4 extends AppCompatActivity {

    static public String EXTRA_PARAMETER1 = "par1";

    static public String EXTRA_PARAMETER2 = "par2";

    static public String RETURN_VALUE = "sum";

    TextView tv1 = null;

    int sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity4);
        tv1 = this.findViewById(R.id.textView1);

        int par1 = 0;
        int par2 = 0;

        if (getIntent().hasExtra(EXTRA_PARAMETER1)) {
            par1 = getIntent().getIntExtra(EXTRA_PARAMETER1,0);
        }

        if (getIntent().hasExtra(EXTRA_PARAMETER2)) {
            par2 = getIntent().getIntExtra(EXTRA_PARAMETER2,0);
        }

        sum = par1 + par2;

        tv1.setText(par1 + "+" + par2 + "=" + sum);

    }

    @Override
    public void onBackPressed(){
        Intent in = new Intent();
        in.putExtra(RETURN_VALUE,  sum);
        setResult(0, in);
        super.onBackPressed();
    }

}
