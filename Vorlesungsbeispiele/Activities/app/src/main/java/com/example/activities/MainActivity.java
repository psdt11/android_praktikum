package com.example.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity  {

    TextView tv1 = null;
    TextView tv2 = null;
    TextView tv3 = null;
    EditText et1 = null;
    EditText et2 = null;
    EditText et3 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = this.findViewById(R.id.textView1);
        tv2 = this.findViewById(R.id.textView2);
        tv3 = this.findViewById(R.id.textView3);
        et1 = this.findViewById(R.id.editText1);
        et2 = this.findViewById(R.id.editText2);
        et3 = this.findViewById(R.id.editText3);
        tv1.setText(tv1.getText() + "\r\n" + "onCreate()");
    }

    @Override
    protected void onStart(){
        super.onStart();
        tv1.setText(tv1.getText() + "\r\n" + "onStart()");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        tv1.setText(tv1.getText() + "\r\n" + "onRestart()");
    }


    @Override
    protected void onResume(){
        super.onResume();
        tv1.setText(tv1.getText() + "\r\n" + "onResume()");
    }

    @Override
    protected void onPause(){
        super.onPause();
        tv1.setText(tv1.getText() + "\r\n" + "onPause()");
    }

    @Override
    protected void onStop(){
        super.onStop();
        tv1.setText(tv1.getText() + "\r\n" + "onStop()");
    }

    public void openDialog(View v){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle( "Hallo" )
                .setMessage("Kurze Unterbrechung")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    public void openActivityAsDialog(View v){
        Intent intent=new Intent(this, DialogActivity.class);
        startActivity(intent);
    }

    public void openSecondActivity(View v){
        Intent intent=new Intent(this, Activity1.class);
        startActivity(intent);
    }

    public void openThirdActivity(View v) {
        Intent intent=new Intent(this, Activity2.class);
        intent.putExtra(Activity2.EXTRA_PARAMETER, et1.getText().toString());
        startActivity(intent);
    }

    public void openFourthActivity(View v) {
        Intent intent=new Intent(this, Activity3.class);
        // startActivityForResult(intent,0 ); != startActivity(intent);
        startActivityForResult(intent,0 );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==0 && resultCode == 0) {
            tv2.setText(data.getCharSequenceExtra(Activity3.RETURN_VALUE));
        }

        if (requestCode==1 && resultCode == 0) {
            tv3.setText(String.valueOf(data.getIntExtra(Activity4.RETURN_VALUE,0)));
        }

    }

    public void openFifthActivity(View v) {
        Intent intent=new Intent(getApplicationContext(), Activity4.class);
        intent.putExtra(Activity4.EXTRA_PARAMETER1, Integer.parseInt(et2.getText().toString()));
        intent.putExtra(Activity4.EXTRA_PARAMETER2, Integer.parseInt(et3.getText().toString()));
        startActivityForResult(intent,1);
    }

    public void openExternalActivity(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "+496421"));
        startActivity(intent);;
    }

    public void deleteLog(View v) {
        tv1.setText("");
    }

}
