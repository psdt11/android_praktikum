package com.example.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    static public String EXTRA_PARAMETER = "name";

    TextView tv = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        tv = this.findViewById(R.id.textView);

        if (getIntent().hasExtra(EXTRA_PARAMETER)) {
            String name = getIntent().getStringExtra(EXTRA_PARAMETER);
            tv.setText(name);
        }
    }
}
