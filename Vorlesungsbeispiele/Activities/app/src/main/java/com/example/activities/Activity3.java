package com.example.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class Activity3 extends AppCompatActivity {

    static public String RETURN_VALUE = "name";

    EditText et = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity3);
        et = this.findViewById(R.id.editText);
    }

    public void closeActivity(View v) {
        onBackPressed();
    }

    @Override
    public void onBackPressed(){
        Intent in = new Intent();
        in.putExtra(RETURN_VALUE,  et.getText().toString());
        setResult(0, in);
        super.onBackPressed();
    }
}
