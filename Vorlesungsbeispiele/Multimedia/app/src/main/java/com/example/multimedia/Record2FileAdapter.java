package com.example.multimedia;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.io.File;
import java.io.FilenameFilter;

public class Record2FileAdapter extends ArrayAdapter<File> {

    private final Context context;

    Record2FileAdapter(Context context){
        super(context, android.R.layout.simple_list_item_1);
        this.context = context;
        findAndAddFiles();
    }


    private void findAndAddFiles() {

        FilenameFilter fnf = new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                if (!s.toLowerCase().endsWith(Record2File.EXT_3GP)){
                    return false;
                }
                File f = new File(file, s);
                return f.canRead() && !f.isDirectory();
            }
        };

        File d = RecordActivity.getBaseDir(context);
        File [] files = d.listFiles(fnf);
        if (files != null) {
            for (File f:files){
                add(new Record2File(f.getParentFile(), f.getName()));
            }
        }
    }
}
