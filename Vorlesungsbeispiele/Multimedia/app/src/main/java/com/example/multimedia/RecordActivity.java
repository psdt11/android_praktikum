package com.example.multimedia;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;

public class RecordActivity extends AppCompatActivity {

    private static final String TAG = RecordActivity.class.getSimpleName();

    private static final int PERMISSION_RECORD_AUDIO = 123;

    private enum MODE{
        WAITING, RECORDING, PLAYING
    }

    private MODE mode;

    private File currentFile;

    private MediaPlayer mediaPlayer;

    private Record2FileAdapter listAdapter;

    private MediaRecorder mediaRecorder;

    Button bt = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        bt = findViewById(R.id.button);
        ListView lv = findViewById(R.id.listview);
        listAdapter = new Record2FileAdapter(this);
        lv.setAdapter(listAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                File f = listAdapter.getItem(i);
                if (f!=null){
                    playAudioFile(f.getAbsolutePath());
                }
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l){
                File f = listAdapter.getItem(i);
                if (f!=null){
                    if (f.delete()){
                        listAdapter.remove(f);
                    }
                }
                return true;
            }
        });
        currentFile = null;
        mode = MODE.WAITING;
        mediaPlayer = null;
        mediaRecorder = null;
    }

    public void record(View v){

        if (mode == MODE.RECORDING) {
            bt.setText("Aufnahme starten");
            mode = MODE.WAITING;
            mediaRecorder.stop();
            releaseRecorder();
            listAdapter.add(currentFile);
            currentFile = null;
        } else if (mode == MODE.WAITING) {
            currentFile = recordToFile();
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        if(checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            bt.setEnabled(false);
            requestPermissions(new String[] {Manifest.permission.RECORD_AUDIO}, PERMISSION_RECORD_AUDIO);
        } else {
            bt.setEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if ((requestCode==PERMISSION_RECORD_AUDIO) && (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)){
            bt.setEnabled(true);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        releasePlayer();
        releaseRecorder();
    }

    private File recordToFile(){
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        //File f = new Record2File(getBaseDir(this), Long.toString(System.currentTimeMillis())+Record2File.EXT_3GP);
        File f = new File(getExternalFilesDir(Environment.DIRECTORY_MUSIC), Long.toString(System.currentTimeMillis())+Record2File.EXT_3GP);
        try {
            if (!f.createNewFile()){
                Log.d(TAG, "Datei schon vorhanden");
            }
            mediaRecorder.setOutputFile(f.getAbsolutePath());
            mediaRecorder.prepare();
            mediaRecorder.start();
            mode = MODE.RECORDING;
            bt.setText("Aufnahme läuft ..");
            return f;
        } catch (IOException e){
            Log.e(TAG, "Konnte Aufnahme nicht starten", e);
        }
        return null;
    }

    private void releaseRecorder(){
        if (mediaRecorder != null){
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    private void playAudioFile(String filename){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                releasePlayer();
                mode = MODE.WAITING;
                bt.setText("Aufnahme starten");
            }
        });
        try {
            mediaPlayer.setDataSource(filename);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mode = MODE.PLAYING;
            bt.setText("Aufnahme wird abgespielt");
        } catch (Exception thr) {
            Log.e(TAG, "Konnte Audio nicht wiedergeben", thr);
        }
    }

    private void releasePlayer(){
        if (mediaPlayer!= null){
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public static File getBaseDir(Context ctx){
        File dir = new File(ctx.getExternalFilesDir(null), ".RecordActiviy");
        if (!dir.mkdirs()){
            Log.d(TAG, "Verzeichnis schon vorhanden");
        }
        return dir;
    }
}
