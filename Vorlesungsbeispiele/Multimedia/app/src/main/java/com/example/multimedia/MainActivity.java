package com.example.multimedia;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tonaufzeichnung (View v){
        Intent intent = new Intent(this, RecordActivity.class);
        startActivity(intent);
    }

    public void text2speech(View v){
        Intent intent = new Intent (this, Text2SpeechActivity.class);
        startActivity(intent);
    }

    public void speech2text(View v){
        Intent intent = new Intent (this, Speech2TextActivity.class);
        startActivity(intent);
    }

    public void snapshot(View v){
        Intent intent = new Intent (this, FotoActivity.class);
        startActivity(intent);
    }

    public void video(View v){
        Intent intent = new Intent (this, VideoActivity.class);
        startActivity(intent);
    }
}
