package com.example.multimedia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.File;
import java.util.Hashtable;
import java.util.Locale;

public class Text2SpeechActivity extends AppCompatActivity {

    private static final int RQ_CHECK_TTS_DATA = 1;

    private static final String TAG = Text2SpeechActivity.class.getSimpleName();

    private final Hashtable<String, Locale> supportedLanguages = new Hashtable<>();

    private TextToSpeech tts;
    private String last_utterance_id;

    private EditText et;
    private Spinner sp;
    private Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tts = null;
        Intent intent = new Intent();
        intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, RQ_CHECK_TTS_DATA);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (tts != null){
            tts.shutdown();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_CHECK_TTS_DATA) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int i) {
                        onInit1(i);
                    }
                });
            }
        } else {
            Intent installIntent = new Intent();
            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
            startActivity(installIntent);
            finish();
        }
    }


    public void onInit1(int status){
        if (status != TextToSpeech.SUCCESS) {
            finish();
        } else {
            setContentView(R.layout.activity_text2speech);
            et = findViewById(R.id.editText);
            sp = findViewById(R.id.spinner);
            bt = findViewById(R.id.button2);
            tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String s) {
                    Log.d(TAG, "onStart():" + last_utterance_id);
                }

                @Override
                public void onDone(String s) {
                    final Handler h = new Handler(Looper.getMainLooper());
                    final String uid = s;
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            if (uid.equals(last_utterance_id)){
                                bt.setEnabled(true);
                            }
                        }
                    });
                }

                @Override
                public void onError(String s) {
                    Log.d(TAG, "onError():" + s);
                }
            });

            String [] languages = Locale.getISOLanguages();
            for (String lang : languages){
                Locale loc = new Locale(lang);
                switch (tts.isLanguageAvailable(loc)){
                    case TextToSpeech.LANG_MISSING_DATA:
                    case TextToSpeech.LANG_NOT_SUPPORTED:
                        break;
                    default:
                        String key = loc.getDisplayLanguage();
                        if(!supportedLanguages.contains(key)){
                            supportedLanguages.put(key, loc);
                        }
                        break;
                }
            }
        }
        ArrayAdapter<Object> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, supportedLanguages.keySet().toArray());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        sp.setAdapter(adapter);
    }

    public void text2speech(View v){
        String text = et.getText().toString();
        String key = (String) sp.getSelectedItem();
        Locale loc = supportedLanguages.get(key);
        if (loc != null) {
            bt.setEnabled(false);
            tts.setLanguage(loc);
            last_utterance_id = Long.toString(System.currentTimeMillis());
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, last_utterance_id);
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PODCASTS), last_utterance_id + ".wav");
            tts.synthesizeToFile(text, null, file, last_utterance_id);
            Log.d(TAG, file.getAbsolutePath());
        }
    }
}
