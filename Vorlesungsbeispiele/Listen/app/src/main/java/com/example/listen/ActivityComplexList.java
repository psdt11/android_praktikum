package com.example.listen;

import android.app.LauncherActivity;
import android.content.ContentProviderClient;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivityComplexList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complexlist);
        ListView lv = (ListView) findViewById(R.id.list);

        // Erzeugen von ListItems
        final ArrayList<ListItem> eintraege_liste = new ArrayList<ListItem>();
        for (int i=0; i<15; i++){
            eintraege_liste.add(new ListItem(true, "Eintrag " + (15-i)));
        }

        final AdvancedAdapter advancedAdapter = new AdvancedAdapter(this, eintraege_liste);
        lv.setAdapter(advancedAdapter);

        lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ActivityComplexList.this, "Zur Detailansicht von " + advancedAdapter.getItem(position).getTitle() + " springen ...", Toast.LENGTH_LONG).show();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                advancedAdapter.remove(advancedAdapter.getItem(position));
                return true;
            }
        });
    }
}
