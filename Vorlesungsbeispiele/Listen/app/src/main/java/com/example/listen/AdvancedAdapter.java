package com.example.listen;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

public class AdvancedAdapter extends ArrayAdapter<ListItem> {

    public AdvancedAdapter(Context context, ArrayList<ListItem> list){
        super(context, 0, list);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View element = convertView;
        if (element == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            element = inflater.inflate(R.layout.advanced_list_item, null);
        }

        Switch switchv =(Switch) element.findViewById(R.id.switch1);
        switchv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked && getItem(position).isChecked()) {
                    getItem(position).setTitle(getItem(position).getTitle() + " (erledigt)");
                    getItem(position).setChecked(false);
                }
                if (isChecked && !getItem(position).isChecked()) {
                    getItem(position).setTitle(getItem(position).getTitle().replaceAll(" \\(erledigt\\)", "").toString());
                    getItem(position).setChecked(true);
                }
                notifyDataSetChanged();
            }
        });

        ImageView iv = (ImageView) element.findViewById(R.id.imageView);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(new ListItem(false, "Eintrag " + (getCount()+1)));
            }
        });

        TextView tv = (TextView) element.findViewById(R.id.textView);
        switchv.setChecked(getItem(position).isChecked());
        tv.setText(getItem(position).getTitle());
        return element;

    }

}
