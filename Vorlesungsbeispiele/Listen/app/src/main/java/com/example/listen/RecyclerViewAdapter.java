package com.example.listen;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private String[] mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout cl;
        public Switch switchv;
        public TextView tv;

        public MyViewHolder(ConstraintLayout v) {
            super(v);
            cl = v;
            switchv = cl.findViewById(R.id.switch1);
            tv = cl.findViewById(R.id.textView);
        }

        public Switch getSwitchview() {
            return switchv;
        }

        public TextView getTextView() {
            return tv;
        }
    }

    public RecyclerViewAdapter(String[] myDataset) {
        super();
        mDataset = myDataset;

    }

    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.advanced_list_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.getTextView().setText(mDataset[position]);
        holder.getSwitchview().setChecked(true);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
