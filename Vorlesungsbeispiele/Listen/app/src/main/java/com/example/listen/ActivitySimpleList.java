package com.example.listen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class ActivitySimpleList extends AppCompatActivity {

    String[] eintraege = {
            "Eintrag 1",
            "Eintrag 2",
            "Eintrag 3",
            "Eintrag 4",
            "Eintrag 5",
            "Eintrag 6",
            "Eintrag 7",
            "Eintrag 8",
            "Eintrag 9",
            "Eintrag 10",
            "Eintrag 11",
            "Eintrag 12",
            "Eintrag 13",
            "Eintrag 14",
            "Eintrag 15",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simplelist);
        final ArrayList<String> eintraege_liste =new ArrayList<String>(Arrays.asList(eintraege));

        //Listview adapter
        ListView lv = this.findViewById(R.id.listview);
        // Built-in (android.*): android.R.layout.simple_list_item_1
        final ArrayAdapter<String> array_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, eintraege_liste);
        lv.setAdapter(array_adapter);


        lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(ActivitySimpleList.this, "Ihre Auswahl : " + eintraege_liste.get(position) + " (id=" + id + ")", Toast.LENGTH_LONG).show();
                if (!eintraege_liste.get(position).contains("*")) {
                    eintraege_liste.set(position, "*" + eintraege_liste.get(position));
                } else {
                    eintraege_liste.set(position, eintraege_liste.get(position).replace("*", ""));
                }
                array_adapter.notifyDataSetChanged();
            }
        });


        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                eintraege_liste.remove(position);
                array_adapter.notifyDataSetChanged();
                return true;
            }
        });

        Button bt = new Button(this);
        bt.setText("Eintrag einfügen");
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eintraege_liste.add("Eintrag " + eintraege_liste.size());
                array_adapter.notifyDataSetChanged();
            }
        });
        lv.addFooterView(bt);

    }

}
