package com.example.listen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class ActivityRecyclerViewList  extends AppCompatActivity {

    String[] eintraege = {
            "Eintrag 1",
            "Eintrag 2",
            "Eintrag 3",
            "Eintrag 4",
            "Eintrag 5",
            "Eintrag 6",
            "Eintrag 7",
            "Eintrag 8",
            "Eintrag 9",
            "Eintrag 10",
            "Eintrag 11",
            "Eintrag 12",
            "Eintrag 13",
            "Eintrag 14",
            "Eintrag 15",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerviewlist);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        RecyclerView.Adapter  recyclerviewAdapter = new RecyclerViewAdapter(eintraege);
        recyclerView.setAdapter(recyclerviewAdapter);
        //https://developer.android.com/guide/topics/ui/layout/recyclerview#java

    }

}
