package com.example.listen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void simpleListActivity(View v){
        Intent in = new Intent(this, ActivitySimpleList.class);
        startActivity(in);
    }

    public void complexListActivity(View v){
        Intent in = new Intent(this, ActivityComplexList.class);
        startActivity(in);
    }

    public void recyclerViewListActivity(View v){
        Intent in = new Intent(this, ActivityRecyclerViewList.class);
        startActivity(in);
    }

}
