package com.example.webbrowser;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    WebView wv = null;
    EditText et = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        wv = findViewById(R.id.webview);
        et = findViewById(R.id.editText);
        /*
        Button bt = findViewById(R.id.button);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadPage(view);
            }
        });
        */

        /*
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    loadPage (v);
                    return true;
                }
                return false;
            }
        });
        */

    }

    @Override
    public void onBackPressed() {
        wv.goBack();
    }

    public void loadPage (View view) {

        wv.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                et.setText(wv.getUrl());
            }

        });

        String url = et.getText().toString();

        if (!(url.contains("http://") || url.contains("https://"))) {
            url = "http://" + url;
            et.setText(url);
        }
        wv.loadUrl(url);

        /*
        try  {
            wv.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
        */
    }
}
