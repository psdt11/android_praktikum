package pis.hue2.client;

import javafx.application.Platform;
import pis.hue2.server.LaunchServer;

import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Clientseitige GUI
 */
public class ClientGUI extends Application {

    /**
     * LaunchClient-Objekt initialisiert mit Verbindung zum Server
     */
    private LaunchClient lc = new LaunchClient(this);

    /**
     * main-Methode, startet die GUI
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * TextArea zur Anzeige der Nachrichten zum und vom Server
     */
    TextArea taCons = new TextArea();
    /**
     * TextArea zur Anzeige der Dateiliste
     */
    TextArea taList = new TextArea();
    /**
     * TextField zur Eingabe des Dateinamens fuer Anfragen an den Server
     */
    private TextField tffilename = new TextField();
    /**
     * TextField zur Eingabe eines Ports
     */
    private TextField tfPort = new TextField();
    /**
     * TextField zur Eingabe eines Hosts
     */
    private TextField tfHost = new TextField();
    /**
     * Button fuer Connect
     */
    private Button btnCON;
    /**
     * Button fuer Disconnect
     */
    private Button btnDSC;
    /**
     * Button fuer Anforderung der Dateiliste
     */
    private Button btnLST;
    /**
     * Button fuer Ablegen einer Datei auf dem Server
     */
    private Button btnPUT;
    /**
     * Button fuer Herunterladen einer Datei vom Server
     */
    private Button btnGET;
    /**
     * Button fuer Loeschung einer Datei auf dem Server
     */
    private Button btnDEL;

    /**
     * Baut die GUI auf
     */
    public void start(Stage Stage1) {

        Stage1.setTitle("Messager Client");
        GridPane root = new GridPane();
        root.setPadding(new Insets(10, 10, 10, 10));
        taCons.setPromptText("Console");
        tffilename.setPromptText("File Name");
        tfPort.setPromptText("Port");
        root.add(tfPort, 1, 1);
        tfHost.setPromptText("Host");
        root.add(tfHost, 1, 2);
        root.add(taCons, 0, 0);
        root.add(taList, 1, 0);
        Label lblFN = new Label("Enter file name");
        lblFN.setLabelFor(tffilename);
        root.add(lblFN, 0, 13);
        root.add(tffilename, 0, 14);
        Label lblCON = new Label("Connect to server");
        lblCON.setLabelFor(btnCON);
        root.add(lblCON, 0, 1);
        btnCON = new Button();
        btnCON.setText("CON");
        btnCON.setOnAction(CONClick());
        root.add(btnCON, 0, 2);
        Label lblDSC = new Label("Disconnect from server");
        lblDSC.setLabelFor(btnDSC);
        root.add(lblDSC, 0, 3);
        btnDSC = new Button();
        btnDSC.setText("DSC");
        btnDSC.setOnAction(DSCClick());
        btnDSC.setDisable(true);
        root.add(btnDSC, 0, 4);
        Label lblLST = new Label("List Files on server");
        lblLST.setLabelFor(btnLST);
        root.add(lblLST, 0, 5);
        btnLST = new Button();
        btnLST.setText("LST");
        btnLST.setOnAction(LSTClick());
        btnLST.setDisable(true);
        root.add(btnLST, 0, 6);
        Label lblPUT = new Label("Put a file on the server");
        lblPUT.setLabelFor(btnPUT);
        root.add(lblPUT, 0, 7);
        btnPUT = new Button();
        btnPUT.setText("PUT");
        btnPUT.setOnAction(PUTClick(Stage1));
        btnPUT.setDisable(true);
        root.add(btnPUT, 0, 8);
        Label lblGET = new Label("Get file from server");
        lblGET.setLabelFor(btnGET);
        root.add(lblGET, 0, 9);
        btnGET = new Button();
        btnGET.setText("GET");
        btnGET.setOnAction(GETClick(Stage1));
        btnGET.setDisable(true);
        root.add(btnGET, 0, 10);
        Label lblDEL = new Label("Delete file from server");
        lblDEL.setLabelFor(btnDEL);
        root.add(lblDEL, 0, 11);
        btnDEL = new Button();
        btnDEL.setText("DEL");
        btnDEL.setOnAction(DELClick());
        btnDEL.setDisable(true);
        root.add(btnDEL, 0, 12);
        Stage1.setScene(new Scene(root, 700, 700));
        Stage1.show();
    }

    /**
     * ClickEvent fuer CON (Connect)
     *
     * @return EventHandler
     */
    private EventHandler<ActionEvent> CONClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int port = 0;
                boolean validPort;
                try {
                    port = Integer.parseInt(tfPort.getText());
                    validPort = true;
                } catch (NumberFormatException e) {
                    validPort = false;
                }

                if (validPort && !tfHost.getText().equals("")) {
                    lc.setPort(port);
                    lc.setHost(tfHost.getText());

                    Runnable r = new Runnable() {
                        public void run() {
                            try {
                                disableAllBtn(true);
                                if (!lc.con()) {
                                    Runnable popup = () -> {
                                        popup("Server full");
                                    };
                                    Platform.runLater(popup);
									disableBtn(false, true, true, true, true, true);
                                } else {
                                    disableBtn(true, false, false, false, false, false);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                Runnable popup = () -> {
                                    popup(e.getMessage());
                                };
                                Platform.runLater(popup);
								disableBtn(false, true, true, true, true, true);
                            }
                        }
                    };
                    Thread t = new Thread(r);
                    t.start();
                } else {
                    Runnable popup = () -> {
                        popup("Enter port and hostname of the server.");
                    };
                    Platform.runLater(popup);
                }
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent fuer DSC (Disconnect)
     *
     * @return EventHandler
     */
    private EventHandler<ActionEvent> DSCClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Runnable r = new Runnable() {
                    public void run() {
                        try {
                            disableAllBtn(true);
                            lc.dsc();
                            disableBtn(false, true, true, true, true, true);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Runnable popup = () -> {
                                popup(e.getMessage());
                            };
                            Platform.runLater(popup);
                        }
                    }
                };
                Thread t = new Thread(r);
                t.start();
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent fuer LST (List)
     *
     * @return EventHandler
     */
    private EventHandler<ActionEvent> LSTClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Runnable r = new Runnable() {
                    public void run() {
                        try {
                            disableAllBtn(true);
                            lc.lst();
                            disableBtn(true, false, false, false, false, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Runnable popup = () -> {
                                popup(e.getMessage());
                            };
                            Platform.runLater(popup);
                        }
                    }
                };
                Thread t = new Thread(r);
                t.start();
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent fuer PUT (Upload)
     *
     * @param s Stage, auf der der FileChooser angezeigt wird
     * @return EventHandler
     */
    private EventHandler<ActionEvent> PUTClick(Stage s) {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!tffilename.getText().equals("")) {
                    final FileChooser fcPath = new FileChooser();
                    File file = fcPath.showOpenDialog(s);
                    Runnable r = new Runnable() {
                        public void run() {
                            try {
                                disableAllBtn(true);
                                if (!lc.put(tffilename.getText(), file)) {
                                    Runnable popup = () -> {
                                        popup("File already existing");
                                    };
                                    Platform.runLater(popup);
                                }
                                disableBtn(true, false, false, false, false, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Runnable popup = () -> {
                                    popup(e.getMessage());
                                };
                                Platform.runLater(popup);
                            }
                        }
                    };
                    Thread t = new Thread(r);
                    t.start();
                } else {
                    Runnable popup = () -> {
                        popup("Please enter file name");
                    };
                    Platform.runLater(popup);
                }
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent fuer GET (Download)
     *
     * @param s Stage, auf der der DirectoryChooser angezeigt wird
     * @return EventHandler
     */
    private EventHandler<ActionEvent> GETClick(Stage s) {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!tffilename.getText().equals("")) {
                    final DirectoryChooser dc = new DirectoryChooser();
                    File dir = dc.showDialog(s);
                    Runnable r = new Runnable() {
                        public void run() {
                            try {
                                disableAllBtn(true);
                                if (!lc.get(tffilename.getText(), dir)) {
                                    Runnable popup = () -> {
                                        popup("File does not exist");
                                    };
                                    Platform.runLater(popup);
                                }
                                disableBtn(true, false, false, false, false, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Runnable popup = () -> {
                                    popup(e.getMessage());
                                };
                                Platform.runLater(popup);
                            }
                        }
                    };
                    Thread t = new Thread(r);
                    t.start();
                } else {
                    Runnable popup = () -> {
                        popup("Please enter file name");
                    };
                    Platform.runLater(popup);
                }
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent fuer DEL (Delete)
     *
     * @return EventHandler
     */
    private EventHandler<ActionEvent> DELClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!tffilename.getText().equals("")) {
                    Runnable r = new Runnable() {
                        public void run() {
                            try {
                                disableAllBtn(true);
                                if (!lc.del(tffilename.getText())) {
                                    Runnable popup = () -> {
                                        popup("File does not exist");
                                    };
                                    Platform.runLater(popup);
                                }
                                disableBtn(true, false, false, false, false, false);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Runnable popup = () -> {
                                    popup(e.getMessage());
                                };
                                Platform.runLater(popup);
                            }
                        }
                    };
                    Thread t = new Thread(r);
                    t.start();
                } else {
                    Runnable popup = () -> {
                        popup("Please enter file name");
                    };
                    Platform.runLater(popup);
                }
            }
        };
        return eventHandler;
    }
    
    /**
     * Methode zum "Ausschalten" einzelner Bottons zur Verhinderung von Fehleingaben
     * 
     * @param con
     * @param dsc
     * @param lst
     * @param put
     * @param get
     * @param del
     */
    private void disableBtn(boolean con, boolean dsc, boolean lst, boolean put, boolean get, boolean del) {
        Runnable r = () -> {
            btnCON.setDisable(con);
            btnDSC.setDisable(dsc);
            btnLST.setDisable(lst);
            btnPUT.setDisable(put);
            btnGET.setDisable(get);
            btnDEL.setDisable(del);
        };
        Platform.runLater(r);
    }

    /**
     * Methode zum "Ausschalten" aller Buttons waehrend auf Serverantwort gewartet wird
     * 
     * @param b
     */
    private void disableAllBtn(boolean b) {
        Runnable r = () -> {
            btnCON.setDisable(b);
            btnDSC.setDisable(b);
            btnPUT.setDisable(b);
            btnGET.setDisable(b);
            btnDEL.setDisable(b);
            btnLST.setDisable(b);
        };

        Platform.runLater(r);
    }

    /**
     * Stellt sicher, dass beim Schliessen des Clients ein Disconnect erfolgt
     */
    public void stop() {
        if (!btnDSC.isDisabled()) {
            try {
                lc.dsc();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fehlermeldungs-Popup
     *
     * @param nachricht Anzuzeigende Nachricht
     */
    private void popup(String nachricht) {
        Alert message = new Alert(Alert.AlertType.ERROR);
        message.setTitle("Fehler!");
        message.setHeaderText(nachricht);
        message.getButtonTypes().setAll(ButtonType.OK);
        message.showAndWait();
    }

}
