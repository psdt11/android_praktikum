package pis.hue2.client;

//import pis.hue2.server.CommunicationThread;
//import pis.hue2.common.Instruction;

import javafx.application.Platform;

import java.io.*;
import java.net.ProtocolException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Stellt den clientseitigen Teil der Kommunikation zwischen Server und Client bereit
 *
 *
 *
 * Nicht threadsicher, da jede GUI einen eigenes Lauchclient-objekt instanziiert
 * und eine verwendendung des ensprechenden LaunchClient-Objektes in mehreren Threads ausschliesst.
 */
public class LaunchClient {
    /**
     * Socket des Clients
     */
    Socket cS;
    /**
     * InputStream von cS
     */
    InputStream iS;
    /**
     * OutputStream von cS
     */
    OutputStream oS;
    /**
     * Clientseitige GUI
     */
    ClientGUI gui;
    /**
     * Hostadresse des Servers
     */
    String host = null;
    /**
     * Portnummer des Servers
     */
    int port = 0;

    /**
     * Konstruktor, initialisiert die cG
     * @param cG clientseitige GUI
     */
    public LaunchClient(ClientGUI cG) {
        this.gui = cG;
    }

    /**
     * setter f&uuml;r den Serverhost, da dieser in der GUI eingelesen wird
     * @param host hostadresse des Servers
     */
    void setHost(String host){
        this.host = host;
    }

    /**
     * setter f&uuml;r den Serverport, da dieser in der GUI eingelesen wird
     * @param port Portnummer des Servers
     */
    void setPort(int port){
        this.port = port;
    }

    /**
     * St&ouml;sst das L&ouml;schen einer Datei auf serverseite und den damit verbundenen Austauschen von Nachrichten an
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antworten des Servers.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @param filename Name der zu l&ouml;schenden Dateo
     * @return true, falls der Server mit ACK antwortet und false bei DND
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */

    boolean del(String filename) throws IOException {
        oS.write(("DEL " + filename + "\n").getBytes());
        System.out.println("(C) DEL");
        printMessage("(C) DEL");

        String instruction = readUntil('\n');

        if (instruction.equals("ACK")){
            printMessage("(S) ACK");
            return true;
        } else if (instruction.equals("DND")){
            printMessage("(S) DND");
            return false;
        } else {
            throw new ProtocolException("''ACK'' or ''DND'' expected");
        }
    }

    /**
     * St&ouml;sst das &uuml;bermitteln einer Datei vom Server an den Client und den damit verbundenen Austauschen von Nachrichten an
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antworten des Servers.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @param filenameServer (serverseitiger) Name der anzufordernden Datei
     * @param dir Ornder, in dem die angeforderte Datei abgelegt werden soll
     * @return true, falls der Server mit ACK antwortet und false bei DND
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */
    boolean get(String filenameServer, File dir) throws IOException {
        oS.write(("GET " + filenameServer + "\n").getBytes());
        System.out.println("(C) GET");
        printMessage("(C) GET");

        String instruction = readUntil('\n');

        if (instruction.equals("ACK")) {
            printMessage("(S) ACK");
            ack();
            if (iSEquals("DAT ")) {
                printMessage("(S) DAT...");
                File f = new File(dir.getAbsolutePath() + "//" + filenameServer);
                f.createNewFile();

                dat_get(f);

                ack();
            } else throw new ProtocolException("''DAT '' expected.");
            return true;
        } else if (instruction.equals("DND")) {
            printMessage("(S) DND");
            return false;
        } else {
            throw new ProtocolException("''DND'' expected.");
        }
    }

    /**
     * St&ouml;sst das &uuml;bermitteln einer Datei vom Server an den Client und den damit verbundenen Austauschen von Nachrichten an
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antworten des Servers.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @param filenameServer serverseitiger Name der abzulegenden Datei
     * @param file Datei die auf dem Server abgelegt werden soll
     * @return true, falls der Server mit ACK antwortet und false bei DND
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */
    boolean put(String filenameServer, File file) throws IOException {
        oS.write(("PUT " + filenameServer + "\n").getBytes());
        System.out.println("(C) PUT");
        printMessage("(C) PUT");
        String instruction = readUntil('\n');

        if (instruction.equals("ACK")) {
            printMessage("(S) ACK");
            dat_put(file);

            if (iSEquals("ACK\n")) {
                printMessage("(S) ACK");
                return true;
            } else {
                throw new ProtocolException("''ACK'' expected");
            }
        } else if (instruction.equals("DND")) {
            printMessage("(S) DND");
            return false;
        } else {
            throw new ProtocolException("''ACK'' or ''DND'' expected");
        }

    }

    /**
     * St&ouml;sst das das Auflisten der beim Server abgelegten Dateien und den damit verbundenen Austauschen von Nachrichten an
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antworten des Servers.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @return String[] mit den einzelenen Dateinamen als Eintr&auml;ge
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */
    String[] lst() throws IOException {
        oS.write("LST\n".getBytes());
        printMessage("(C) LST");

        if (iSEquals("ACK\n")) {
            printMessage("(S) ACK");
            ack();
            if (iSEquals("DAT ")) {
                printMessage("(S) DAT...");
                String[] list = dat_lst();
                printList(list);
                ack();
                return list;
            } else throw new ProtocolException("''DAT '' expected");

        }
        return null;
    }

    /**
     * Bricht die Verbindung ab und schliesst die Streams sowie das Socket. &uuml;bermittelt die mit dem Verbindungsabbruch verbundene Nachricht
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antwort des Servers.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */
    void dsc() throws IOException {
        oS.write("DSC\n".getBytes());
        System.out.println("(C) DSC");
        printMessage("(C) DSC");

        if (iSEquals("DSC\n")) {
            printMessage("(S) DSC");
            iS.close();
            oS.close();
            cS.close();
        } else {
            throw new ProtocolException();
        }
    }

    /**
     * Schrein ACK in den Outputstream und beauftragt die Ausgabe der Nachricht in der GUI
     * @throws IOException falls das Schreiben in den Outputstream fehlschl&auml;gt
     */
    void ack() throws IOException {
        oS.write("ACK\n".getBytes());
        System.out.println("(C) ACK");
        printMessage("(C) ACK");
    }

    /**
     * St&ouml;sst den verbindungsaufbau und den damit verbundenen Austauschen von Nachrichten an
     * und verarbeitet dabei die m&ouml;glichen protokollgerechten Antworten des Servers.
     * Es wird ein neues Socket zur Kommunikation mit dem Server erzeugt und iS sowie oS mit Input- bzw. Outputstream des Sockets belegt.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @return true, falls der Server mit ACK antwortet und false bei DND
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht sind
     */

    boolean con() throws IOException {
        cS = new Socket(host, port);
        System.out.println("Verbunden mit Server: " +
                cS.getRemoteSocketAddress());
        iS = cS.getInputStream();
        oS = cS.getOutputStream();

        oS.write("CON\n".getBytes());
        System.out.println("(C) CON");
        printMessage("(C) CON");

        String instruction = readUntil('\n');

        if (instruction.equals("ACK")) {
            printMessage("(S) ACK");
            return true;
        } else if (instruction.equals("DND")){
            printMessage("(S) DND");
            cS.close();
            return false;
        } else {
            throw new ProtocolException("''ACK'' expected");
        }
    }

    /**
     * &uuml;bermittelt eine protokollgerechte DAT-Nachricht mit dem Inhalt von f als Bytestrom.
     * Ausserdem wird die Ausgabe der Nachrichten in der GUI beauftragt.
     * @param f zu &uuml;bermittelnde Datei
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    void dat_put(File f) throws IOException {
        DataInputStream dIS = new DataInputStream(new FileInputStream(f));
        int messageLength = 0;
        ArrayList<Byte> inputB = new ArrayList<>();

        boolean messageEnd = false;

        oS.write(("DAT ").getBytes());
        printMessage("(C) DAT...");

        while (!messageEnd) {
            try {
                inputB.add(dIS.readByte());
                messageLength++;
            } catch (EOFException e) {
                messageEnd = true;
            }
        }

        dIS.close();

        oS.write(((String.valueOf(messageLength) + " ").getBytes()));

        for (int i = 0; i < messageLength; i++) {
            oS.write(inputB.get(i).byteValue());
        }
        oS.write("\n".getBytes());
    }

    /**
     * Empf&auml;ngt eine protokollgerechte DAT-Nachricht als Antwort auf eine best&auml;tigte LST-Nachricht.
     * @return String[] mit den Namen der beim Server abgelegten Dateien
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antworten des Servers nicht protokollgerecht ist
     */
    String[] dat_lst() throws IOException {
        int messageLength = readMessageLength();
        int numberOfFiles = 0;
        ArrayList<String> list = new ArrayList<>();

        StringBuilder input = new StringBuilder();
        int deletedChars = 0;
        for (int i = 0; i < messageLength; i++) {
            input.append((char) iS.read());
            if (input.charAt(i - deletedChars) == '\n') {
                numberOfFiles++;
                list.add(input.toString());
                input.delete(0, i + 1); //falls i>l&auml;nge wird einfach alles gegl&ouml;scht
                deletedChars = i + 1;
            }
        }

        String[] s = new String[numberOfFiles];

        if (!iSEquals("\n"))
            throw new ProtocolException("''DAT <messagelength> <data>'' must be followed by a line break");

        return list.toArray(s);
    }

    /**
     *  Empf&auml;ngt eine protokollgerechte DAT-Nachricht als Antwort auf eine best&auml;tigte GET-Nachricht.
     * @param f Datei, in die die empfangenen Daten geschrieben werden.
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     * @throws ProtocolException falls die Antwort des Servers nicht protokollgerecht ist
     */
    void dat_get(File f) throws IOException {
        int messageLength = readMessageLength();
        PrintStream pS = new PrintStream(f);

        for (int i = 0; i < messageLength; i++) {
            pS.write(iS.read());
        }
        pS.close();

        if (!iSEquals("\n"))
            throw new ProtocolException("''DAT <messagelength> <data>'' must be followed by a line break");
    }

    /**
     * Liest die entsprechende Anzahl an Bytes des InputStreams und teste, ob diese einer bestimmten Zeichenkette s entsprechen
     * @param s Zeichenkette, der der InputStream entsprechen soll
     * @return true, falls der InputStream s entspricht
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    boolean iSEquals(String s) throws IOException {
        byte[] buffer = new byte[s.length()];
        iS.read(buffer);
        String input = new String(buffer);
        return input.equals(s);
    }

    /**
     * Liest aus dem Inputstream die nach "DAT" in einer Nachricht angegebene L&auml;nge der zu &uuml;bermittelnden Daten
     * @return die L&auml;nge der Nachricht als int.
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    int readMessageLength() throws IOException {
        StringBuilder inputSB = new StringBuilder("");
        boolean messageEnd = false;
        char nextChar;

        while (!messageEnd) {
            try {
                nextChar = (char) iS.read();
                if (nextChar == ' ') {
                    messageEnd = true;
                } else {
                    inputSB.append(nextChar);
                }
            } catch (EOFException e) {
                throw new ProtocolException("''DAT '' must be followed by a string representing a number followed by a white a whitespace");
            }
        }

        int dataLength = 0;
        try {
            dataLength = Integer.parseInt(inputSB.toString());
        } catch (NumberFormatException e) {
            throw new ProtocolException("''DAT '' must be followed by a string representing a number followed by a white a whitespace");
        }
        return dataLength;
    }

    /**
     * Liest den InputStream, bis ein Byte dem &uuml;bergebenen char c entspricht.
     * @param c char bis zu dem gelesen werden soll.
     * @return String, dem der Bytestrom bis exclusive c entspricht.
     * @throws IOException
     */
    String readUntil(char c) throws IOException {
        boolean charFound = false;
        StringBuilder input = new StringBuilder("");
        char nextChar;

        while (!charFound) {
            nextChar = (char) iS.read();
            if (nextChar == '\n') charFound = true;
            else input.append(nextChar);
        }

        return input.toString();
    }

    /**
     * Beauftragt die Ausgabe des Strings s im Konsolentextfeld der GUI.
     * @param s auszugebender String
     */
    private void printMessage(String s){
        Runnable r = () -> {
            gui.taCons.appendText(s+"\n");
        };
        Platform.runLater(r);
    }

    /**
     * Beauftragt die Ausgabe des Strings s im Dateilistentextfeld der GUI.
     * @param list auszugebendes String[]
     */
    private void printList(String[] list){
        Runnable r = () -> {
            gui.taList.setText("List:\n");
            for (int i=0; i<list.length; i++)
                gui.taList.appendText(list[i]);
        };
        Platform.runLater(r);
    }
}
