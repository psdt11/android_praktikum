package pis.hue2.server;

import pis.hue2.common.Instruction;
//import pis.hue2.client.LaunchClient;
//import pis.hue2.server.LaunchServer;


import java.io.*;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketException;

/**
 * Kommunikationsthread zum Nachrichtenaustausch zwischen einem einzelnen Client und dem Server
 */

public class CommunicationThread extends Thread {
    /**
     * Der aufrufende LaunchServer-Thread
     */
    LaunchServer lS;
    /**
     * Um den wie vielten Kommunikationsthread (=verbundenen Client) handelt es sich
     */
    int nthClient;
    /**
     * Kommunikationssocket
     */
    Socket cS;
    /**
     * InputStream des Sockets
     */
    InputStream iS;
    /**
     * Outputstream des Sockets
     */
    OutputStream oS;
    /**
     * Zeigt an, ob die Verbindung zum Client aufgebaut wurde oder neich (z.B. wegen &uuml;berschreitung der maximalen Anzahl an Clients)
     */
    boolean connected;

    /**
     * Pfad des Ordners in dem die Dateien Serverseitig abgelegt werden
     */
    private static final String pathname_dir = "C:\\Users\\Stell\\OneDrive\\Desktop\\TestServer";


    /**
     * Konstruktor
     * @param lS startender LaunchServer-Thread
     * @param clientSocket Socket des Clients, der sich mit dem ServerSocket des startenden LaunchServer-Threads verband
     * @param n Um den wie vielten verbundenen Client es sich handelte
     */
    public CommunicationThread(LaunchServer lS, Socket clientSocket, int n) {
        this.lS = lS;
        nthClient = n;
        this.cS = clientSocket;
        connected = false;
        try {
            iS = cS.getInputStream();
            oS = cS.getOutputStream();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Testet, ob die maximal Anzahl an Clients eingehalten wurde und antwortet entsprechend mit ACK oder DND auf die CON-Anfrage des Clients
     * @return true, falls diese eingehalten wurde und false, falls &uuml;berstiegen
     * @throws IOException falls das Schreiben in den Outputstream eine IOException wirft
     */
    private boolean communicationAcknowledged() throws IOException {

        if (nthClient <= LaunchServer.maxNumberOfClients && iSEquals("CON\n")) {
            ack();
            return true;
        } else {
            dnd();
            dsc();
            return false;
        }
    }

    /**
     * Schleift so lange, bis die interrupted-Flag des Threads zum Beenden gesetzt wird, und liest den Inputstream.
     * Verarbeitet Exceptions, falls der InputStream keine protkollgerechte Anfrage enth&auml;lt.
     */
    @Override
    public void run() {
        boolean communicationAcknowledged = false;
        try {
            communicationAcknowledged = communicationAcknowledged();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (communicationAcknowledged) {
            Instruction instruction;
            String instructionString;

            while (!Thread.interrupted()) {
                try {
                    instructionString = readUntil('\n');
                    instruction = Instruction.valueOf(instructionString.substring(0, 3));

                    switch (instruction) {
                        case DEL: {
                            del(instructionString.substring(4));
                            break;
                        }
                        case DSC: {
                            dsc();
                            break;
                        }
                        case GET: {
                            get(instructionString.substring(4));
                            break;
                        }
                        case LST: {
                            lst();
                            break;
                        }
                        case PUT: {
                            put(instructionString.substring(4));
                            break;
                        }
                        default: {
                            ProtocolException protocolException = new ProtocolException("invalid request");
                            protocolException.printStackTrace();
                        }
                    }
                } catch (IllegalArgumentException e) {
                    ProtocolException protocolException = new ProtocolException("incorrect instruction, cannot convert to Enum Instruction");
                    protocolException.printStackTrace();
                } catch (EOFException e) {
                    ProtocolException protocolException = new ProtocolException("instruction must be ended with a line break");
                    protocolException.printStackTrace();
                } catch (IndexOutOfBoundsException e) {
                    ProtocolException protocolException = new ProtocolException("incorrect instruction - too short, ie missing filename");
                    protocolException.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        LaunchServer.actualNumberOfClients--;
    }


    /**
     * Gibt dem ServiceThread den Auftrag eine Datei zu l&ouml;schen
     * @param filename (serverseitiger) Name der Datei
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    private void del(String filename) throws IOException {
        File f = new File(pathname_dir + "//" + filename);
        if (f.exists()) {
            ack();
            Order order = new Order(this, Task.DEL, f, null);
            try {
                lS.sT.orderQueue.put(order);
            } catch (InterruptedException e) {
                this.interrupt();
            }
        } else dnd();
    }

    /**
     * Gibt dem ServiceThread den Auftrag eine Datei an den Client zu &uuml;bertragen
     * @param filename (serverseitiger) Name der Datei
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    private void get(String filename) throws IOException {
        File f = new File(pathname_dir + "//" + filename);

        if (f.exists()) {
            ack();
            if (iSEquals("ACK\n")) {
                Order order = new Order(this, Task.GET, f, null);
                try {
                    lS.sT.orderQueue.put(order);
                } catch (InterruptedException e) {
                    this.interrupt();
                }

                if (!iSEquals("ACK\n")) throw new ProtocolException("''ACK'' expected.");

            } else throw new ProtocolException("''ACK'' expected.");
        } else dnd();
    }

    /**
     * Gibt dem ServiceThread den Auftrag eine Datei des Clients zu empfangen
     * @param filename (serverseitiger) Name der Datei
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    private void put(String filename) throws IOException {

        File f = new File(pathname_dir + "//" + filename);
        if (!f.exists()) {
            f.createNewFile();
            ack();

            if (iSEquals("DAT ")) {
                int dataLength = readMessageLength();
                byte[] data = new byte[dataLength];

                for (int i=0; i<dataLength; i++){ //nicht direkt komplett data auslesen, weil der Client Thread dann noch ncith fertig mit schrieben ist. Read() ist blockierend.
                    data[i]=(byte) iS.read();
                }

                Order order = new Order(this, Task.PUT, f, data);
                try {
                    lS.sT.orderQueue.put(order);
                    ack();
                } catch (InterruptedException e) {
                    this.interrupt();
                }

                if (!iSEquals("\n")) throw new ProtocolException("''DAT <messagelength> <data>'' must be followed by a line break");
            } else
                throw new ProtocolException("''DAT '' expected"); //Hier abnfangen, dass dann die Datei als neue Nachricht gelsen wird
        } else dnd();

    }

    /**
     * Gibt dem ServiceThread den Auftrag die vorhanenden Dateien aufzulisten
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    private void lst() throws IOException {
        ack();

        if (iSEquals("ACK\n")) {
            File f = new File(pathname_dir);
            Order order = new Order(this, Task.LST, f, null);
            try {
                lS.sT.orderQueue.put(order);
            } catch (InterruptedException e) {
                this.interrupt();
            }

            if (!iSEquals("ACK\n")) throw new ProtocolException("''ACK'' expected");
        } else throw new ProtocolException("''ACK'' expected");

    }

    /**
     * Schreibt DND in den Outputsream
     * @throws IOException falls das Schreiben in den Outputstreams fehlschl&auml;gt
     */
    private void dnd() throws IOException {
        oS.write("DND\n".getBytes());
        System.out.println("(S) DND");
    }

    /**
     * Schreibt DSC in den Outputstream und setzt die Interrupted-Flag des Kommunikationsthreads um diesen zu beenden.
     * @throws IOException falls das Schreiben in den Outputstreams fehlschl&auml;gt
     */
    private void dsc() throws IOException {
        this.interrupt();
        oS.write("DSC\n".getBytes());

        System.out.println("(S) DSC");

        oS.close();
        iS.close();
        cS.close();
    }

    /**
     * Schreibt ACK in den Outputsream
     * @throws IOException falls das Schreiben in den Outputstreams fehlschl&auml;gt
     */
    private void ack() throws IOException {
        oS.write("ACK\n".getBytes());
        System.out.println("(S) ACK");
    }

    /**
     * Liest die entsprechende Anzahl an Bytes des InputStreams und teste, ob diese einer bestimmten Zeichenkette s entsprechen
     * @param s Zeichenkette, der der InputStream entsprechen soll
     * @return true, falls der InputStream s entspricht
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    boolean iSEquals(String s) throws IOException {
        byte[] buffer = new byte[s.length()];
        iS.read(buffer);
        String input = new String(buffer);
        return input.equals(s);
    }

    /**
     * Liest aus dem Inputstream die nach "DAT" in einer Nachricht angegebene L&auml;nge der zu &uuml;bermittelnden Daten
     * @return die L&auml;nge der Nachricht als int.
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    int readMessageLength() throws IOException {
        StringBuilder inputSB = new StringBuilder("");
        boolean messageEnd = false;
        char nextChar;

        while (!messageEnd) {
            try {
                nextChar = (char) iS.read();
                if (nextChar == ' ') {
                    messageEnd = true;
                } else {
                    inputSB.append(nextChar);
                }
            } catch (EOFException e) {
                throw new ProtocolException("''DAT '' must be followed by a string representing a number followed by a white a whitespace");
            }
        }

        int dataLength = 0;
        try {
            dataLength = Integer.parseInt(inputSB.toString());
        } catch (NumberFormatException e) {
            throw new ProtocolException("''DAT '' must be followed by a string representing a number followed by a white a whitespace");
        }

        return dataLength;
    }

    /**
     * Liest den InputStream, bis ein Byte dem &uuml;bergebenen char c entspricht.
     * @param c char bis zu dem gelesen werden soll.
     * @return String, dem der Bytestrom bis exclusive c entspricht.
     * @throws IOException
     */

    String readUntil(char c) throws IOException {
        boolean charFound = false;
        StringBuilder input = new StringBuilder("");
        char nextChar;

        while (!charFound) {
            nextChar = (char) iS.read();
            if (nextChar == '\n') charFound = true;
            else input.append(nextChar);
        }

        return input.toString();
    }


}
