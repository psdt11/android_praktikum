package pis.hue2.server;

import java.io.File;

/**
 * Objekt zur Darstellung der Auftr&auml;ge an den Bedienerthread
 */
public class Order {
    /**
     * auftraggender Kommunikationsthread
     */
    CommunicationThread thread;
    /**
     * auzuf&uuml;hrende Aufgabe des Protocolls
     */
    Task task;
    /**
     * Datei, die dabei verarbeitet werden soll
     */
    File f;
    /**
     * ggf. zu schreibender Inhalt (in Falls von task = PUT). Falls nicht n&ouml;tig null.
     */
    byte[] data;

    /**
     * Konstuktor initialisert entsprechend
     * @param thread auftraggebender Kommunikationsthread
     * @param task auszuf&uuml;hrende Aufgabe
     * @param f zu verarbeitende Datei
     * @param data zu schriebende Daten als byte[]
     */
    public Order(CommunicationThread thread, Task task, File f, byte[] data){
        this.thread = thread;
        this.task = task;
        this.f = f;
        this.data = data;
    }
}
