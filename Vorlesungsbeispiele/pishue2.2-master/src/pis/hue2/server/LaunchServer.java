package pis.hue2.server;

//was auch immer

//import pis.hue2.client.LaunchClient;
//import pis.hue2.server.CommunicationThread;
//import pis.hue2.common.Instruction;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Thread zum Verbindungsaufbau mit den Clients. Dieser Thread startet dann f&uuml;r jeden Client einen Kommunikationsthread.
 */
public class LaunchServer extends Thread{

    /**
     * ServerSocket zum Verbindungsaufbau
     */
    private ServerSocket svS;
    /**
     * Liste zum Verwalten insb. zum Beenden der Threads bei Serverstop
     */
    private static LinkedList<Thread> activeCommunicationThreads = new LinkedList<>();
    /**
     * Anzahl der verbundenen Clients
     */
    static int actualNumberOfClients = 0;
    /**
     * Maximal m&ouml;gliche Anzahl verbundener Clients
     */
    static final int maxNumberOfClients = 3;
    /**
     * Serverport
     */
    private int port;

    /**
     * ServiceThread dient als Flaschenhals zur Threadsicherheit
     */
    ServiceThread sT;


    /**
     * Konstruktor
     * @param port Serverport
     * @throws IOException des Konstruktors des ServerSockets
     */
    public LaunchServer(int port) throws IOException {
        this.port = port;
        svS = new ServerSocket(port);
        svS.setSoTimeout(1000);
        sT = new ServiceThread();
    }

    /**
     *
     * @return port des Servers
     */
    public int getPort() {
        return port;
    }

    /**
     * Schleift so lange, bis der LaunchServer-Thread unterbrochen wird, und wartet jeweils 1000 ms auf eine Verbindung.
     * Wird der Thread unterbrochen, endet die Schleife und alle aktiven Kommunikationsthreads, sowie der Service-Thread werden unterbrochen und so beendet.
     */
    @Override
    public void run() {

        sT.start();

        while (!Thread.interrupted()){
            try {
                Socket clientSocket = svS.accept();
                actualNumberOfClients++;
                int n = actualNumberOfClients;
                System.out.println("New Client: "+actualNumberOfClients + "|"+ n);

                CommunicationThread client = new CommunicationThread(this, clientSocket, n);
                activeCommunicationThreads.add(client);
                client.start();

            } catch (IOException e) {
                if (e.getClass() != java.net.SocketTimeoutException.class)
                    e.printStackTrace();
            }
        }

        System.out.println("interrupted");

        /*synchronized (activeCommunicationThreads) {
            Iterator<Thread> i = activeCommunicationThreads.iterator();
            while (i.hasNext()){
                i.next().interrupt();
            }
        }*/

        for (Thread t :
                activeCommunicationThreads) {
            System.out.println(t+ "   isAlive: "+t.isAlive());
            t.interrupt();
        }

        sT.interrupt();

        try {
            svS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

