package pis.hue2.server;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.SynchronousQueue;

/**
 * Bedienerthread, der als Flaschenhals zur Threadsicherheit die lesenden und schriebeneden Auftr&auml;ge der Kommunikationsthreads bearbeitet.
 */
public class ServiceThread extends Thread {

    /**
     * In der Queue werden die Auftr&auml;ge zum Lesen und Schreieben auf Serverseite abgelegt
     */
    SynchronousQueue<Order> orderQueue = new SynchronousQueue<>();

    /**
     * Schleift so lange, bis die interrupted-Flag des Threads zum Beenden gesetzt wird, und verarbeitet die Auftr&auml;ge der Queue.
     */
    @Override
    public void run(){
        Order order;
        boolean interrupted = false;

        while(!interrupted){
            try {
                order = orderQueue.take();
                switch (order.task){
                    case GET:{
                        try {
                            get(order);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    case LST:{
                        try {
                            lst(order);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    case PUT:{
                        try {
                            put(order);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;

                    }
                    case DEL:{
                        System.out.println("order del received");
                        del(order);
                        break;
                    }
                }

            } catch (InterruptedException e) {
                System.out.println("Service Thread interrupted");
                    interrupted = true;
            }
        }
    }

    /**
     * Liest die in order festgelegte datei (file und byte-Array) aus dm Ordner des Servers. Und schreibt sie als DAT-Nachricht
     * in den OutputStream des in order gespeicherten auftaggenden Kommunikationsthreads.
     * @param order enth&auml;lt Datei-Pfad der zu sendenden Datei und den auftraggenden (und empfangenden) Kommuniakationsthread
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    void get(Order order) throws IOException {

        DataInputStream dIS = new DataInputStream(new FileInputStream(order.f));
        int messageLength = 0;
        ArrayList<Byte> inputB = new ArrayList<>();

        boolean messageEnd = false;
        System.out.println("(Server) DAT");

        order.thread.oS.write(("DAT ").getBytes());

        while (!messageEnd) {
            try {
                inputB.add(dIS.readByte());
                messageLength++;
            } catch (EOFException e) {
                messageEnd = true;
            }
        }

        dIS.close();

        order.thread.oS.write(((String.valueOf(messageLength) + " ").getBytes()));

        for (int i=0; i<messageLength; i++){
            order.thread.oS.write(inputB.get(i).byteValue());
        }

        order.thread.oS.write("\n".getBytes());

    }

    /**
     * List die Dateien, die auf dem Serverabgelegt sind aus und schreibt sie als DAT-Nachricht in den OutputStream des auftraggenden Kommuniakationsthreads
     * @param order enth&auml;lt den auftraggenden Kommuniakationsthread
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    void lst(Order order) throws IOException {
        String[] list = order.f.list();
        int numberOfFiles = list.length;
        int messageLength=0;

        order.thread.oS.write(("DAT ").getBytes());

        for (int i=0; i<numberOfFiles; i++){
            messageLength += (list[i].length()+1); //+1 f&uuml;r den zeilenumbruch
        }

        order.thread.oS.write(((String.valueOf(messageLength) + " ").getBytes()));

        for (int i=0; i<numberOfFiles; i++){
            order.thread.oS.write((list[i]+"\n").getBytes());
        }

        order.thread.oS.write("\n".getBytes());

    }

    /**
     * Schriebt die in order festgelegte datei (file und byte-Array) in den Ordner des Servers. Und schreibt in den OutputStream des in order gespeicherten
     * auftaggenden Kommunikationsthreads
     * @param order enth&auml;lt Datei-Pfad und -Inhalt (als byte[]) der anzulegenden Datei und den auftraggenden Kommuniakationsthread
     * @throws IOException falls das Lesen oder Schreiben des Input- bzw. Outputstreams fehlschl&auml;gt
     */
    void put(Order order) throws IOException {
        int messageLength=0;
        int dataLength = order.data.length;
        order.f.createNewFile();

        PrintStream pS = new PrintStream(order.f);

        for (int i=0; i<dataLength; i++){
            pS.write(order.data[i]);
        }

        pS.close();

    }

    /**
     * L&ouml;scht die in order festgelegte Datei in den Ordner des Servers.
     * @param order enth&auml;lt Datei-Pfad
     */
    void del(Order order){
        order.f.delete();
    }

}
