package pis.hue2.server;

import javafx.application.Platform;
import pis.hue2.server.LaunchServer;

import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.*;
/**
 *Serverseitige GUI
 */
public class ServerGUI extends Application {
	/**
	 * LaunchServer-Objekt, auf das ueber die GUI zugegriffen wird
	 */
    private LaunchServer ls;
    /**
     * TextField zum Einstellen des Ports
     */
    private TextField tfPort;
    /**
     * Button zum Start des Servers
     */
    private Button btnStart;
    /**
     * Button zum Beenden des Servers
     */
    private Button btnStop;

    /**
     * Baut die GUI auf
     */
    public void start(Stage Stage1) {
        Stage1.setTitle("Messager Server Client");
        GridPane root = new GridPane();
        root.setPadding(new Insets(10, 10, 10, 10));
        tfPort = new TextField();
        tfPort.setPromptText("Port");
        root.add(tfPort, 0, 0);
        btnStart = new Button();
        btnStart.setText("Start");
        btnStart.setOnAction(StartClick());
        root.add(btnStart, 0, 1);
        btnStop = new Button();
        btnStop.setText("Stop");
        btnStop.setOnAction(StopClick());
        btnStop.setDisable(true);
        root.add(btnStop, 0, 2);
        Stage1.setScene(new Scene(root, 150, 150));
        Stage1.show();
    }

    /**
     * ClickEvent zum Stoppen des Servers
     * 
     * @return EventHandler
     */
    private EventHandler<ActionEvent> StopClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ls.interrupt();
                btnStart.setDisable(false);
                btnStop.setDisable(true);
            }
        };
        return eventHandler;
    }

    /**
     * ClickEvent zum Start des Servers
     * 
     * @return EventHandler
     */
    private EventHandler<ActionEvent> StartClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int port = 0;
                boolean validPort;
                try {
                    port = Integer.parseInt(tfPort.getText());
                    validPort = true;
                } catch (NumberFormatException e) {
                    validPort = false;
                }

                if (validPort) {
                    try {
                        ls = new LaunchServer(port);
                        ls.start();
                        btnStart.setDisable(true);
                        btnStop.setDisable(false);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Runnable popup = () -> {
                            popup(e.getMessage());
                        };
                        Platform.runLater(popup);
                    }
                } else {
                    Runnable popup = () -> {
                        popup("Enter a port.");
                    };
                    Platform.runLater(popup);
                }
            }
        };
        return eventHandler;
    }
    
    /**
     * Fehlermeldungs-Popup
     *
     * @param nachricht Anzuzeigende Nachricht
     */
    private void popup(String nachricht) {
        Alert message = new Alert(Alert.AlertType.ERROR);
        message.setTitle("Fehler!");
        message.setHeaderText(nachricht);
        message.getButtonTypes().setAll(ButtonType.OK);
        message.showAndWait();
    }

    /**
     * Stellt sicher, dass beim Schliessen der Gui der Server gestoppt wird
     */
    @Override
    public void stop() {
        if (!btnStop.isDisabled()) ls.interrupt();
    }

    /**
     * main-Methode, startet die GUI
     */
    public static void main(String[] args) {
        launch(args);
    }

}
