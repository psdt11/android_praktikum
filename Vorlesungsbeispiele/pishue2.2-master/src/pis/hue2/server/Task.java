package pis.hue2.server;

/**
 * Teilmenge von Nachrichten des in Instruction festgelgten Protokolls, die ein Lesen bzw. Schreiben des serverseitigen Speichers erfordern
 * @see pis.hue2.common.Instruction
 */
public enum Task {
    GET,
    PUT,
    LST,
    DEL
}
