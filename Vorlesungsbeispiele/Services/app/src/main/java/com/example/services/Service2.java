package com.example.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Binder;

public class Service2 extends Service {

    private final IBinder mBinder = new LocalBinder();

    class LocalBinder extends Binder {
        Service2 getService(){
            return Service2.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent){
        return mBinder;
    }

    public int fakultaet(int n) {
        if (n<=0) {
            return 1;
        }
        return n* fakultaet(n-1);
    }
}
