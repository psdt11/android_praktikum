package com.example.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Service2Activity extends AppCompatActivity {

    private Service2 mService = null;

    private TextView textView;
    private EditText editText;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Service2.LocalBinder binder = (Service2.LocalBinder) iBinder;
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service2activity);
        textView = findViewById(R.id.textview);
        editText = findViewById(R.id.editText);
    }

    public void click(View v){
        int n = Integer.parseInt(editText.getText().toString());
        int fak = mService.fakultaet(n);
        textView.setText(String.valueOf(fak));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, Service2.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mService != null){
            unbindService(mConnection);
            mService = null;
        }
    }

}
