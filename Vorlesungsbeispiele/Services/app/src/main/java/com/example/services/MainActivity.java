package com.example.services;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int RQ_CALL_LOG = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(checkSelfPermission(Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[] {Manifest.permission.READ_CALL_LOG}, RQ_CALL_LOG);
        } else {
            startServiceAndFinish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if(requestCode == RQ_CALL_LOG){
            if((grantResults.length>0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                startServiceAndFinish();
            } else {
                Toast.makeText(this, "Denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startServiceAndFinish(){
        Intent intent = new Intent(this, Service1.class);
        startService(intent);
        //startForegroundService(intent);
        //finish();
    }

    public void startService2Activity(View v){
        Intent intent = new Intent(this, Service2Activity.class);
        startActivity(intent);
    }

    public void startService3Activity(View v){
        Intent intent = new Intent(this, Service3Activity.class);
        startActivity(intent);
    }
}
