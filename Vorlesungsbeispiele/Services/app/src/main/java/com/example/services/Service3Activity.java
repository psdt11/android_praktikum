package com.example.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Service3Activity extends AppCompatActivity {

    private static String TAG = Service3Activity.class.getSimpleName();

    public static final int MSG_FAKULTAET_IN = 1;
    public static final int MSG_FAKULTAET_OUT = 2;

    private TextView textView;
    private EditText editText;

    private Messenger mService = null;
    Messenger mMessenger = null;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service3activity);
        textView = findViewById(R.id.textview);
        editText = findViewById(R.id.editText);
        mMessenger = new Messenger(new IncomingHandler(textView));
    }

    public void click(View v){
        if (mService != null){
            try{
                int n = Integer.parseInt(editText.getText().toString());
                Message msg = Message.obtain(null, MSG_FAKULTAET_IN, n, 0);
                msg.replyTo =  mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                Log.d(TAG, "send()", e);
            }
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        ComponentName c = new ComponentName(
                "com.example.remoteservice",
                "com.example.remoteservice.Service3"
        );
        Intent intent = new Intent();
        intent.setComponent(c);
        if (!bindService(intent, mConnection, Context.BIND_AUTO_CREATE)){
            Log.d(TAG, "bindService() nicht erfolgreich");
            unbindService(mConnection);
            mService = null;
            finish();
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        if(mService != null) {
            unbindService(mConnection);
            mService = null;
        }
    }

    private static class IncomingHandler extends Handler {

        private TextView tv = null;

        private IncomingHandler(TextView tv) {
            this.tv = tv;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_FAKULTAET_OUT:
                    int n = msg.arg1;
                    int fakultaet = msg.arg2;
                    Log.d(TAG, "Fakultaet: " + fakultaet);
                    tv.setText(String.valueOf(n) + "! = " + String.valueOf(fakultaet));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    }
}
