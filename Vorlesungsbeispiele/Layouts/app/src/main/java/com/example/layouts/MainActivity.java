package com.example.layouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        for (int i=0; i<100; i++){
            TextView textView = new TextView(this);
            textView.setText(i + " Hello World!");
            linearLayout.addView(textView);
        }
        setContentView(linearLayout);
         */
        setContentView(R.layout.activity_main);
    }
}
