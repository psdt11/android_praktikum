package com.example.dateioperationen;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;

public class DateiSchreibenActivity extends AppCompatActivity {

    public final String FILENAME = "Block22.txt";

    public static TextView tv;
    public static EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datei);
        Button bt = findViewById(R.id.button6);
        bt.setText("Hinzufügen und Speichern");
        tv = findViewById(R.id.textView2);
        tv.setText("");
        et = findViewById(R.id.editText4);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText(tv.getText() + "\n" + et.getText().toString());
                et.setText("");
                try {
                    FileOutputStream fos = openFileOutput(FILENAME, MODE_PRIVATE);
                    fos.write(tv.getText().toString().getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        };

        bt.setOnClickListener(ocl);
    }

}
