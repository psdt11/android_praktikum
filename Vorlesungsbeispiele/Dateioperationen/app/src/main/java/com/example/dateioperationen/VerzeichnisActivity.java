package com.example.dateioperationen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;

public class VerzeichnisActivity extends AppCompatActivity {

    TextView tv;
    EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verzeichnis);
        et = findViewById(R.id.editText2);

        Button bt1 = findViewById(R.id.button11);
        View.OnClickListener ocl1 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File folder = new File(getFilesDir()+"//"+et.getText().toString());
                if (!folder.exists()) folder.mkdir();
                listfiles();
            }
        };
        bt1.setOnClickListener(ocl1);

        Button bt2 = findViewById(R.id.button13);

        View.OnClickListener ocl2 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletefiles();
                listfiles();
            }
        };
        bt2.setOnClickListener(ocl2);

        tv = findViewById(R.id.textView4);

        listfiles();
    }

    private void listfiles(){
        tv.setText("");
        for (File file1 : getFilesDir().listFiles()){
            tv.setText(tv.getText().toString() + file1.toString() + "\n");
        }
    }

    private void deletefiles(){
        for (File file1 : getFilesDir().listFiles()){
            file1.delete();
        }
    }
}
