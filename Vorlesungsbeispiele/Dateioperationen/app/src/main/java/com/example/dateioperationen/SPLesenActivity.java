package com.example.dateioperationen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;

public class SPLesenActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "BLOCK22";
    public static Switch sw;
    public static EditText et;
    public static SeekBar sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp);
        Button bt = findViewById(R.id.button3);

        bt.setText("Lesen");

        sw = findViewById(R.id.switch1);
        et = findViewById(R.id.editText);
        sb = findViewById(R.id.seekBar);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
                sw.setChecked(sp.getBoolean("sw", false));
                et.setText(sp.getString("et", "Kein Wert gefunden!"));
                sb.setProgress(sp.getInt("sb", 0));
            }
        };

        bt.setOnClickListener(ocl);
    }
}
