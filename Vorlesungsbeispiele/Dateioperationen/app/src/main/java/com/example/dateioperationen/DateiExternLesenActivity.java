package com.example.dateioperationen;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DateiExternLesenActivity extends AppCompatActivity {

    public final String FILENAME = "Block22.txt";

    public static TextView tv;
    public static EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datei);
        Button bt = findViewById(R.id.button6);
        bt.setText("Datei auslesen.");
        tv = findViewById(R.id.textView2);
        tv.setText("");
        et = findViewById(R.id.editText4);
        et.setVisibility(View.INVISIBLE);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    File sdcard = getExternalFilesDir(null);
                    File file = new File(sdcard, FILENAME);
                    FileInputStream fis = new FileInputStream(file);
                    int length = fis.available();
                    String result = "";
                    for (int i=0; i<length; i++){
                        result = result + (char) fis.read();
                    }
                    tv.setText(result);
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        };

        bt.setOnClickListener(ocl);
    }
}
