package com.example.dateioperationen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void spschreiben (View v){
        Intent intent = new Intent(this, SPSchreibenActivity.class);
        startActivity(intent);
    }

    public void splesen (View v){
        Intent intent = new Intent(this, SPLesenActivity.class);
        startActivity(intent);
    }

    public void dateischreiben (View v){
        Intent intent = new Intent(this, DateiSchreibenActivity.class);
        startActivity(intent);
    }

    public void dateilesen (View v){
        Intent intent = new Intent(this, DateiLesenActivity.class);
        startActivity(intent);
    }

    public void dateiexternschreiben (View v){
        Intent intent = new Intent(this, DateiExternSchreibenActivity.class);
        startActivity(intent);
    }

    public void dateiexternlesen (View v){
        Intent intent = new Intent(this, DateiExternLesenActivity.class);
        startActivity(intent);
    }

    public void verzeichnis (View v){
        Intent intent = new Intent(this, VerzeichnisActivity.class);
        startActivity(intent);
    }
}
