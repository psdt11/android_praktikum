package com.example.dateioperationen;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class DateiLesenActivity extends AppCompatActivity {

    public final String FILENAME = "Block22.txt";

    public static TextView tv;
    public static EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datei);
        Button bt = findViewById(R.id.button6);
        bt.setText("Datei auslesen.");
        tv = findViewById(R.id.textView2);
        tv.setText("");
        et = findViewById(R.id.editText4);
        et.setVisibility(View.INVISIBLE);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fis = openFileInput(FILENAME);
                    int length = fis.available();
                    String result = "";
                    for (int i=0; i<length; i++){
                        result = result + (char) fis.read();
                    }
                    tv.setText(result);
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        };

        bt.setOnClickListener(ocl);
    }
}
