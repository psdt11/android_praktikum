package com.example.dateioperationen;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class DateiExternSchreibenActivity extends AppCompatActivity {

    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;
    String state=Environment.getExternalStorageState();

    public final String FILENAME = "Block22.txt";

    public static TextView tv;
    public static EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datei);
        Button bt = findViewById(R.id.button6);
        bt.setText("Hinzufügen und Speichern");
        tv = findViewById(R.id.textView2);
        tv.setText("");
        et = findViewById(R.id.editText4);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText(tv.getText() + "\n" + et.getText().toString());
                et.setText("");
                checkState();
                if (mExternalStorageWriteable) {
                    try {
                        File sdcard = getExternalFilesDir(null);
                        File file = new File(sdcard, FILENAME);
                        FileOutputStream fos = new FileOutputStream(file);
                        fos.write(tv.getText().toString().getBytes());
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        bt.setOnClickListener(ocl);
    }

    private void checkState(){
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)){
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
    }

}