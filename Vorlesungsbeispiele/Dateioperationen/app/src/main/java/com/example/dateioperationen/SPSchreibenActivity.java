package com.example.dateioperationen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

public class SPSchreibenActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "BLOCK22";
    public static Switch sw;
    public static EditText et;
    public static SeekBar sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp);
        Button bt = findViewById(R.id.button3);

        bt.setText("Schreiben");

        sw = findViewById(R.id.switch1);
        et = findViewById(R.id.editText);
        sb = findViewById(R.id.seekBar);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor  sp_editor = sp.edit();
                sp_editor.putBoolean("sw" , SPSchreibenActivity.sw.isChecked());
                sp_editor.putString("et", SPSchreibenActivity.et.getText().toString());
                sp_editor.putInt("sb", SPSchreibenActivity.sb.getProgress());
                sp_editor.commit();
                Toast toast = Toast.makeText(getApplicationContext(), "Gespeichert!", Toast.LENGTH_SHORT);
                toast.show();;
            }
        };

        bt.setOnClickListener(ocl);
    }
}
