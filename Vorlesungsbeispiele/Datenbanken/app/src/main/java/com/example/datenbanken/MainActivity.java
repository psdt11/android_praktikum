package com.example.datenbanken;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {

    private OpenHandler openHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ImageButton buttonGut = findViewById(R.id.gut);
        buttonGut.setOnClickListener((e) ->
                imageButtonClicked(OpenHandler.MOOD_FINE)
        );

        final ImageButton buttonOk = findViewById(R.id.ok);
        buttonOk.setOnClickListener((e) ->
                imageButtonClicked(OpenHandler.MOOD_OK)
        );

        final ImageButton buttonSchlecht = findViewById(R.id.schlecht);
        buttonSchlecht.setOnClickListener((e) ->
                imageButtonClicked(OpenHandler.MOOD_BAD)
        );

        final Button buttonAuswertung = findViewById(R.id.auswertung);
        buttonAuswertung.setOnClickListener((e) -> {
                Intent intent = new Intent(this, HistoryListActivity.class);
                startActivity(intent);
        });

        openHandler = new OpenHandler(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        openHandler.close();
    }

    private void imageButtonClicked(int mood) {
        openHandler.insert(mood, System.currentTimeMillis());
        Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT).show();
    }
}
