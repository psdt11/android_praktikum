package com.example.umrechner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /*
    *   Spinner sind die Drop-Down Menüs
    */
    private Spinner menu_spinner = null;
    private Spinner spinner_1 = null;
    private Spinner spinner_2 = null;

    ArrayAdapter menuAdapter = null;
    ArrayAdapter dataAdapter_length = null;
    ArrayAdapter dataAdapter_temp = null;

    /*
    *   Indices für das aktuell ausgewählte Element im Drop-Down Menü
    */
    int cur_sel_1 = 0;
    int cur_sel_2 = 0;

    /*
    *   Textfelder zum bearbeiten der Eingabe
    */
    private EditText number_one = null;
    private TextView number_two = null;

    /*
    *   Knöpfe für für die manuelle Berechnung
    */
    private Button button_up = null;
    private Button button_down = null;

    /*
    * Liste für das Übermenü
    */

    private List menu;

    /*
    *   Listen für verschiedene Überkategorien für das Umwandeln von Zahlen
    */
    private List length;
    private List temp;
    private List imperial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        *   Die globalen Items werden über die ID zugewiesen
        */
        button_up = (Button) findViewById(R.id.button_up);
        button_down = (Button) findViewById(R.id.button_down);

        number_one = (EditText) findViewById(R.id.number_one);
        number_two = (TextView) findViewById(R.id.number_two);

        menu_spinner = (Spinner) findViewById(R.id.menu_spinner);
        spinner_1 = (Spinner) findViewById(R.id.spinner);
        spinner_2 = (Spinner) findViewById(R.id.spinner2);

        /*
        * Beim Erstellen des ersten Views werden die Texte auf '0' gesetzt um zu gewährleisten, dass die App nicht ausversehen
        * durch die Listener abstürzt
        */
        number_one.setText("0");
        number_two.setText("0");

        /*
        *   Die Listen werden mit ihren dazugehörigen Einheiten befüllt
        */
        createLists();

        /*
        *   Die Dropdown Menüs werden mit den Listen verknüpft, damit diese angezeigt werden können.
        */

        menuAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, menu);
        menuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        menu_spinner.setAdapter(menuAdapter);

        dataAdapter_length = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, length);
        dataAdapter_length.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_1.setAdapter(dataAdapter_length);
        spinner_2.setAdapter(dataAdapter_length);
        cur_sel_1 = spinner_1.getSelectedItemPosition();
        cur_sel_2 = spinner_2.getSelectedItemPosition();

        dataAdapter_temp = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, temp);
        dataAdapter_temp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        /*
        *   Die verschiedenen Elemente werden mit Listenern verknüpft
        */
        createListener();
    }

    private void createLists() {

        menu = new ArrayList();
        menu.add("Längen");
        menu.add("Temperaturen");

        length = new ArrayList();
        length.add("km");
        length.add("m");
        length.add("cm");

        temp = new ArrayList();
        temp.add("°C");
        temp.add("°F");
    }

    /*
    *   Berechnungsmethode für den Button mit dem Pfeil nach unten bzw. vom oberen Textfeld in das untere
    */
    void calc_down() {
        double one = Double.parseDouble(number_one.getText().toString());
        double result = 0;
        if(String.valueOf(spinner_1.getSelectedItem()).equals("°F") && String.valueOf(spinner_2.getSelectedItem()).equals("°C")) {
            result = (one -32) * check_first();
        } else if(String.valueOf(spinner_1.getSelectedItem()).equals("°C") && String.valueOf(spinner_2.getSelectedItem()).equals("°F")) {
            result = one * check_first() + 32;
        } else {
            result = one * check_first();
        }
        number_two.setText(String.valueOf(result));
    }

    /*
    *   Berechnungsmethode für den Button mit dem Pfeil nach oben bzw. vom unteren Textfeld in das obere
    */
    void calc_up() {
        double two = Double.parseDouble(number_two.getText().toString());
        double result = 0;
        if(String.valueOf(spinner_2.getSelectedItem()).equals("°F") && String.valueOf(spinner_1.getSelectedItem()).equals("°C")) {
            result = (two -32) * check_second();
        } else if(String.valueOf(spinner_2.getSelectedItem()).equals("°C") && String.valueOf(spinner_1.getSelectedItem()).equals("°F")) {
            result = two * check_second() + 32;
        } else {
            result = two * check_second();
        }
        number_one.setText(String.valueOf(result));
    }

    double check_first() {
        String first = String.valueOf(spinner_1.getSelectedItem());
        String second = String.valueOf(spinner_2.getSelectedItem());
        System.out.println("[Check First]: First = " + first +" , Second = " + second);
        return check(first, second);
    }

    double check_second() {
        String first = String.valueOf(spinner_1.getSelectedItem());
        String second = String.valueOf(spinner_2.getSelectedItem());
        System.out.println("[Check Second]: First = " + first +" , Second = " + second);
        return check(second,first);
    }

    private void createListener() {
        number_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (!number_one.getText().toString().equals("")) {
                    calc_down();
                }
            }
        });
        /*number_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                calc_up();
            }
        });*/

        button_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc_down();
            }
        });

        button_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc_up();
            }
        });

        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!String.valueOf(menu_spinner.getSelectedItem()).equals("Längen")) {
                    spinner_1.setAdapter(dataAdapter_temp);
                    spinner_2.setAdapter(dataAdapter_temp);
                } else if(!String.valueOf(menu_spinner.getSelectedItem()).equals("Temperaturen")) {
                    spinner_1.setAdapter(dataAdapter_length);
                    spinner_2.setAdapter(dataAdapter_length);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!String.valueOf(number_one.getText().toString()).equals("")) {
                    if (cur_sel_1 != position) {
                        calc_up();
                    }
                    cur_sel_1 = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        spinner_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(cur_sel_2 != position) {
                    calc_down();
                }
                cur_sel_2 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

    }

    private double check(String first, String second) {
        double factor = 0;
        switch (first) {
            case "km":
                switch(second) {
                    case "km":
                        factor = 1;
                        break;
                    case "m":
                        factor = 1000;
                        break;
                    case "cm":
                        factor = 100000;
                        break;
                }
                break;
            case "m":
                switch(second) {
                    case "km":
                        factor = 0.001;
                        break;
                    case "m":
                        factor = 1;
                        break;
                    case "cm":
                        factor = 100;
                        break;
                }
                break;
            case "cm":
                switch(second) {
                    case "km":
                        factor = 0.00001;
                        break;
                    case "m":
                        factor = 0.01;
                        break;
                    case "cm":
                        factor = 1;
                        break;
                }
                break;
            case "°C":
                switch (second) {
                    case "°C":
                        factor = 1;
                        break;
                    case "°F":
                        factor = 1.8;
                        break;
                }
                break;
            case "°F":
                switch (second) {
                    case "°C":
                        factor = 1 / 1.8;
                        break;
                    case "°F":
                        factor = 1;
                        break;
                }
                break;
        }
        System.out.println("[Check]: Factor = " + factor);
        return factor;
    }
}