package com.example.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.content.Intent;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<ListItem> tdArray = new ArrayList<>();
    AdvancedAdapter advancedAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView lv = (ListView) findViewById(R.id.listview);

        tdArray = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            tdArray.add(new ListItem(false, "Eintrag " + (15 - i), "Inhalt " + (15 - i), i+1, 5, 2022));
        }

        advancedAdapter = new AdvancedAdapter(this, tdArray);
        lv.setAdapter(advancedAdapter);

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //Intent intent = new Intent();
                //advancedAdapter.remove(advancedAdapter.getItem(position));
                Intent in = new Intent(getApplicationContext(), AddActivity.class);
                in.putExtra(AddActivity.LIST_ITEM_TITLE, advancedAdapter.getItem(position).getTitle());
                in.putExtra(AddActivity.LIST_ITEM_CONTENT, advancedAdapter.getItem(position).getContent());
                in.putExtra(AddActivity.LIST_ITEM_DAY, advancedAdapter.getItem(position).getDay());
                in.putExtra(AddActivity.LIST_ITEM_MONTH, advancedAdapter.getItem(position).getMonth());
                in.putExtra(AddActivity.LIST_ITEM_YEAR, advancedAdapter.getItem(position).getYear());
                in.putExtra(AddActivity.LIST_ITEM_POSITION, position);
                startActivityForResult(in, 2);
                return true;
            }
        });
    }

    public void openAddActivity(View v) {
        Intent intent = new Intent(this, AddActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 1) {
            tdArray.add(new ListItem(false, data.getStringExtra(AddActivity.LIST_ITEM_TITLE),
                    data.getStringExtra(AddActivity.LIST_ITEM_CONTENT),
                            data.getIntExtra(AddActivity.LIST_ITEM_DAY, 1),
                            data.getIntExtra(AddActivity.LIST_ITEM_MONTH, 1),
                            data.getIntExtra(AddActivity.LIST_ITEM_YEAR, 1900)));
            advancedAdapter.notifyDataSetChanged();
        }
        if (requestCode == 2 && resultCode == 1){
            int pos = data.getIntExtra(AddActivity.LIST_ITEM_POSITION, -1);
            advancedAdapter.getItem(pos).setTitle(data.getStringExtra(AddActivity.LIST_ITEM_TITLE));
            advancedAdapter.getItem(pos).setContent(data.getStringExtra(AddActivity.LIST_ITEM_CONTENT));
            advancedAdapter.getItem(pos).setDay(data.getIntExtra(AddActivity.LIST_ITEM_DAY, 0));
            advancedAdapter.getItem(pos).setMonth(data.getIntExtra(AddActivity.LIST_ITEM_MONTH, 0));
            advancedAdapter.getItem(pos).setYear(data.getIntExtra(AddActivity.LIST_ITEM_YEAR, 0));
        }
    }
}
