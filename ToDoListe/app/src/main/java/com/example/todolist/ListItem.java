package com.example.todolist;

public class ListItem {

    private boolean checked;
    private String title;
    private String content;
    private int day;
    private int month;
    private int year;

    public ListItem(boolean checked, String title, String content, int day, int month, int year) {
        this.checked = checked;
        this.title = title;
        this.content = content;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent(){return content;}

    public void setContent(String content){this.content = content;}

    public int getYear() { return year; }

    public void setYear(int year) { this.year = year; }

    public int getDay() { return day; }

    public void setDay(int day) { this.day = day; }

    public int getMonth() { return month; }

    public void setMonth(int month) { this.month = month; }
}
