package com.example.todolist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class AddActivity extends AppCompatActivity {

    static public String LIST_ITEM_TITLE = "title";
    static public String LIST_ITEM_CONTENT = "content";
    static public String LIST_ITEM_DAY = "day";
    static public String LIST_ITEM_MONTH = "month";
    static public String LIST_ITEM_YEAR = "year";
    static public String LIST_ITEM_POSITION = "position";

    EditText etTitle = null;
    EditText etContent = null;
    EditText etDay = null;
    EditText etMonth = null;
    EditText etYear = null;
    Button btOk = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        etTitle = this.findViewById(R.id.editTextTitle);
        etContent = this.findViewById(R.id.editTextContent);
        etDay = this.findViewById(R.id.editTextDay);
        etMonth = this.findViewById(R.id.editTextMonth);
        etYear = this.findViewById(R.id.editTextYear);
        btOk = this.findViewById(R.id.button);

        String title;
        String content;
        int day;
        int month;
        int year;

        if(getIntent().hasExtra(LIST_ITEM_POSITION)){
            if (getIntent().hasExtra(LIST_ITEM_TITLE)) {
                title = getIntent().getStringExtra(LIST_ITEM_TITLE);
                etTitle.setText(title);
            }
            if (getIntent().hasExtra(LIST_ITEM_CONTENT)) {
                content = getIntent().getStringExtra(LIST_ITEM_CONTENT);
                etContent.setText(content);
            }
            if (getIntent().hasExtra(LIST_ITEM_DAY)){
                day = getIntent().getIntExtra(LIST_ITEM_DAY, 1);
                etDay.setText(Integer.toString(day));
            }
            if (getIntent().hasExtra(LIST_ITEM_MONTH)){
                month = getIntent().getIntExtra(LIST_ITEM_MONTH, 1);
                etMonth.setText(Integer.toString(month));
            }
            if (getIntent().hasExtra(LIST_ITEM_YEAR)){
                year = getIntent().getIntExtra(LIST_ITEM_YEAR, 1900);
                etYear.setText(Integer.toString(year));
            }
        }




        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent();
                in.putExtra(LIST_ITEM_TITLE, etTitle.getText().toString());
                in.putExtra(LIST_ITEM_CONTENT, etContent.getText().toString());
                in.putExtra(LIST_ITEM_DAY, Integer.parseInt(etDay.getText().toString()));
                in.putExtra(LIST_ITEM_MONTH, Integer.parseInt(etMonth.getText().toString()));
                in.putExtra(LIST_ITEM_YEAR, Integer.parseInt(etYear.getText().toString()));
                in.putExtra(LIST_ITEM_POSITION, getIntent().getIntExtra(LIST_ITEM_POSITION, -2));
                setResult(1,in);
                AddActivity.super.onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(0);
        AddActivity.super.onBackPressed();
    }
}


