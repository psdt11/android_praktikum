package com.example.todolist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class AdvancedAdapter extends ArrayAdapter<ListItem> {

    public AdvancedAdapter(Context context, ArrayList<ListItem> list){
        super(context, 0, list);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View element = convertView;
        if (element == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            element = inflater.inflate(R.layout.special_list_item, null);
        }

        Button bt = (Button) element.findViewById(R.id.buttonCheck);

        if(getItem(position).isChecked()){
            bt.setText("Uncheck");
        }else{
            bt.setText("Check");
        }

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItem(position).setChecked(!getItem(position).isChecked());
                if (getItem(position).isChecked()){
                    bt.setText("Uncheck");
                }else{
                    bt.setText("Check");
                }
                notifyDataSetChanged();
            }
        });

        Button btErase = (Button) element.findViewById(R.id.buttonErase);

        btErase.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                if(!getItem(position).isChecked()){
                    String[] options = {"Yes", "No"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Really?");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0){
                                remove(getItem(position));
                            }
                        }
                    });
                    builder.show();
                }else{
                    remove(getItem(position));
                }
                notifyDataSetChanged();
            }
        });

        TextView tv = (TextView) element.findViewById(R.id.textViewTitle);
        TextView tvDate = (TextView) element.findViewById(R.id.textViewDate);
        tv.setText(getItem(position).getTitle());
        tvDate.setText(getItem(position).getDay()+ "." + getItem(position).getMonth() + "." +getItem(position).getYear());

        return element;
    }
}
